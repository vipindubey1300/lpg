import { createStore,combineReducers } from "redux";
import userReducer from "../reducers/userReducer";
import apiReducer from "../reducers/apiReducer";

const mainReducer = combineReducers({
    userReducer,
    apiReducer
  })

const store = createStore(mainReducer);


export default store;






