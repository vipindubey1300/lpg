import React, { useEffect, useState } from 'react';

import {  useSelector, useDispatch } from 'react-redux';
import { NavigationContainer,DefaultTheme } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,Alert} from 'react-native';

import Home from '../screens/Home.js';
import Notifications from '../screens/Notifications.js';
import { colors } from '../utils/constants.js';
import ContactAdmin from '../screens/ContactAdmin.js';
import Terms from '../screens/Terms.js';
import Orders from '../screens/Orders.js';
import TrackOrder from '../screens/TrackOrder.js';
import Account from '../screens/Account.js';
import Location from '../screens/Location.js';
import Booking from '../screens/Booking.js';


//navigators


const Stack = createStackNavigator()


const HomeNavigator = () => {
     
  return (
    
      <Stack.Navigator initialRouteName="home" mode='modal'>
        <Stack.Screen name="home" component={Home} options={{headerShown:false}} />
       
        <Stack.Screen name='location' component={Location} 
         options = {{
          title:"Choose Location",
          headerTintColor:'white',
          headerBackTitleVisible:false,
          headerTitleStyle:{color:'white'},
          headerStyle:{backgroundColor:colors.COLOR_PRIMARY}
        }}/>

    <Stack.Screen name='booking' component={Booking} 
         options = {{
          title:"Booking",
          headerTintColor:'white',
          headerBackTitleVisible:false,
          headerTitleStyle:{color:'white'},
          headerStyle:{backgroundColor:colors.COLOR_PRIMARY}
        }}/>


        <Stack.Screen name='notifications' component={Notifications} 
         options = {{
          //headerTransparent: true,
          title:"Notifications",
          headerTintColor:'white',
          headerBackTitleVisible:false,
          headerTitleStyle:{color:'white'},
          headerStyle:{backgroundColor:colors.COLOR_PRIMARY}
        }}/>


      <Stack.Screen name='contact_admin' component={ContactAdmin} 
         options = {{
          title:"Contact Admin",
          headerTintColor:'white',
          headerBackTitleVisible:false,
          headerTitleStyle:{color:'white'},
          headerStyle:{backgroundColor:colors.COLOR_PRIMARY}
        }}/>


    <Stack.Screen name='terms' component={Terms} 
         options = {{
          title:"Terms and Conditions",
          headerTintColor:'white',
          headerBackTitleVisible:false,
          headerTitleStyle:{color:'white'},
          headerStyle:{backgroundColor:colors.COLOR_PRIMARY}
        }}/>

    <Stack.Screen name='orders' component={Orders} 
         options = {{
          title:"My Orders",
          headerTintColor:'white',
          headerBackTitleVisible:false,
          headerTitleStyle:{color:'white'},
          headerStyle:{backgroundColor:colors.COLOR_PRIMARY}
        }}/>  

      <Stack.Screen name='track_order' component={TrackOrder} 
         options = {{
          title:"Track Order",
          headerTintColor:'white',
          headerBackTitleVisible:false,
          headerTitleStyle:{color:'white'},
          headerStyle:{backgroundColor:colors.COLOR_PRIMARY}
        }}/>  


    <Stack.Screen name='account' component={Account} 
         options = {{
          title:"My Account",
          headerTintColor:'white',
          headerBackTitleVisible:false,
          headerTitleStyle:{color:'white'},
          headerStyle:{backgroundColor:colors.COLOR_PRIMARY}
        }}/>  


      </Stack.Navigator>
   
  )
}

export default HomeNavigator