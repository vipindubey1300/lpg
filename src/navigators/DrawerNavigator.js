import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {createDrawerNavigator } from '@react-navigation/drawer';


import CustomDrawer from '../screens/CustomDrawer.js';



import HomeNavigator from './HomeNavigator';


const DrawerNavigator = (props) => {

    const Drawer = createDrawerNavigator();
    return(
        
            <Drawer.Navigator
             drawerContent={(props) => <CustomDrawer {...props} />} >
                <Drawer.Screen 
                     component = {HomeNavigator}
                     name="Home"
                    
                />
                
            </Drawer.Navigator>
        
    )
}

export default DrawerNavigator;