import React, { useEffect, useState } from 'react';

import {  useSelector, useDispatch } from 'react-redux';
import { NavigationContainer,DefaultTheme } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,Alert} from 'react-native';

import Login from '../screens/Login.js';
import SignUp from '../screens/SignUp.js';

import Otp from '../screens/Otp.js';
import ResetPassword from '../screens/ResetPassword.js';
import DrawerNavigator from './DrawerNavigator';
import ForgotPassword from '../screens/ForgotPassword.js';
import Terms from '../screens/Terms.js';


//navigators


const Stack = createStackNavigator()


const LoginNavigator = () => {
     
  return (
   
      <Stack.Navigator initialRouteName="login" mode='modal' options={{ gesturesEnabled: true,}}>
        <Stack.Screen name="login" component={Login} options ={{ headerShown :false }} />
        <Stack.Screen name="home" component={DrawerNavigator} />
        <Stack.Screen name="signup" component={SignUp}  options = {{title:"Sign Up"  , headerBackTitleVisible:false, headerTintColor:'black'}} />
        <Stack.Screen name="forgot_password" component={ForgotPassword} options = {{title:"Forgot Password"  , headerBackTitleVisible:false, headerTintColor:'black'}}/>
        <Stack.Screen name="otp" component={Otp} options = {{title:"OTP"  , headerBackTitleVisible:false, headerTintColor:'black'}}/>
        <Stack.Screen name='terms' component={Terms} 
         options = {{
          title:"Terms and Conditions",
          headerTintColor:'black',
          headerBackTitleVisible:false,
        }}/>
        <Stack.Screen name="reset_password" component={ResetPassword} options = {{title:"Reset Password"  , headerBackTitleVisible:false, headerTintColor:'black'}} />

      </Stack.Navigator>
  
  )
}

export default LoginNavigator