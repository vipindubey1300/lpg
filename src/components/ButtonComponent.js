import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import {colors,urls,dimensions} from '../utils/constants';



 const ButtonComponent = (props) => {

  function _onPress()  {
    props.handler()
  }
  
  return (
    <TouchableOpacity style={[styles.container,props.style]}
    onPress={()=> _onPress() }>
      

        <Text style={[{fontWeight:'bold',color:colors.COLOR_SECONDARY,fontSize:17},props.textStyle]}>{props.label.toUpperCase()}</Text>

            
     </TouchableOpacity>
  )
};

ButtonComponent.defaultProps = {

  handler: () => {},
  style: {},
  
};

export default ButtonComponent

const styles = StyleSheet.create({

  container:{
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY,
    paddingHorizontal:15,
    paddingVertical:10,
    borderRadius:10,
    marginTop:20,
    marginBottom:10,borderWidth:0.7,
    borderColor:colors.COLOR_PRIMARY

    
  }
})
