import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'


import {colors,urls,dimensions} from '../utils/constants';



const CustomHeader  : () => React$Node = (props) => {


    function _onPress()  {
        props.navigation.goBack()
      }
      
      return (
       <View style={styles.container}>
            <TouchableOpacity onPress={_onPress}>
                <FastImage 
                source={require('../assets/back-icon.png')}
                style={styles.headerImage} 
                resizeMode={FastImage.resizeMode.cover}/>
            </TouchableOpacity>
    
             <Text style={styles.labelText}>{props.label}</Text>
    
       </View>
      )
};

CustomHeader.defaultProps = {
};


const styles = StyleSheet.create({

    container:{
      flexDirection:'row',
     
      alignItems:'center',
      backgroundColor:colors.COLOR_PRIMARY,
      padding:10,
      width:dimensions.SCREEN_WIDTH,
      height:50,
        //ios    
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        },
        //android
        elevation: 2
    },
    labelText:{
      fontWeight:'400',
      color:colors.WHITE,
     // marginRight:dimensions.SCREEN_WIDTH * 0.04,
      fontSize:19,
      marginHorizontal:20
    },
    headerImage:{
        height:20,width:20
    }
  })

export default CustomHeader;
