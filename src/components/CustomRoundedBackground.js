import React ,{useState,useEffect} from 'react';
import {View,Image,Text,StyleSheet, Dimensions} from 'react-native';
//import Color from '../utility/Color';

const CustomRoundedBackground = (props) => {

  
    return(
        (props.viewStyle == 1
        ?
        <View style={[props.outerStyle ,{alignSelf:"flex-start",backgroundColor}]}>
          
                
                <View style={styles.containerGreenLeft}>
                    <Text style={styles.titleText}>{props.title}</Text>
                </View>
           
            <View style={[styles.containerWhiteLeft,props.containerWhiteHeight]}>
                {props.children}
            </View>
        </View>
        :
     
        <View  style={[props.outerStyle ,{position:'relative',alignSelf:"flex-start",}]}>
            <View style={{flexDirection:"row-reverse",alignSelf:"flex-end",zIndex:100,}}>
                <View style={[styles.viewLineRight,props.viewLineHeight]} />
                <View style={[styles.containerGreenRight,props.containerGreenRight]}>
                    <Text style={styles.titleText}>{props.title}</Text>
                </View>
                <View style={[styles.containerWhiteRight,props.containerWhiteHeight]}>
                   {props.children}
                   {/* <Text>dbj</Text> */}
                </View>
             
            </View>
               
                
              
        </View>
        
       
        )
        
    )

}

const styles = StyleSheet.create({
    containerGreenLeft :{
        zIndex:80,
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:15,
        paddingRight:15,
        alignSelf:"flex-start",
        borderTopRightRadius:20,
        borderBottomRightRadius:20,
        backgroundColor:'green',
        flexWrap:'nowrap',
        maxWidth:Dimensions.get('window').width - 50
      
    },
    containerWhiteLeft :{
        zIndex:70,
        height:'auto',
        position:'absolute',
        left:5,
        top:20,
        padding:20,
        alignSelf:"flex-start",
        borderTopRightRadius:25,
        borderBottomRightRadius:25,
        maxWidth:Dimensions.get('window').width - 40,
        backgroundColor:'white',borderLeftColor:'green',borderLeftWidth:3
    },
    viewLineLeft:{
        width:0,
        height:100,
        borderColor:'green',
        borderWidth:0,
        alignSelf:'flex-start',
    },
    titleText:{
        fontFamily:'montserrat-bold',
        color:"white",
        fontSize:13
    },
    containerGreenRight :{
        zIndex:80,
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:15,
        paddingRight:15,
        position:'absolute',
        borderTopLeftRadius:20,
        borderBottomLeftRadius:20,
        backgroundColor:'green',
        flexWrap:'nowrap',
        maxWidth:Dimensions.get('window').width - 60,
      
    },
    containerWhiteRight :{
        zIndex:70,
        height:80,
        position:'absolute',
        left:5,
        top:20,
        padding:20,
        //alignSelf:"flex-start",
       // width:'auto',
        borderTopLeftRadius:25,
        borderBottomLeftRadius:25,
        maxWidth:Dimensions.get('window').width-40,
        backgroundColor:'white',
       // alignSelf:'flex-end',
    },
    viewLineRight:{
        width:2,
        height:100,
        borderColor:'green',
        borderWidth:2,
        alignSelf:'flex-end',
    },
})

export  default CustomRoundedBackground;