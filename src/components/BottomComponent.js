import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'


import {colors,urls,dimensions} from '../utils/constants';



const BottomComponent  : () => React$Node = (props) => {


    function _onPress()  {
        props.navigation.goBack()
      }
      
      return (
        <View style={styles.bottomContainer}>
          <TouchableOpacity onPress={()=> props.navigation.navigate('account')}>
          <FastImage 
            source={require('../assets/usericon.png')}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.contain}/>
          </TouchableOpacity>


          <TouchableOpacity style={styles.elevatedButton}>
          <FastImage 
            source={require('../assets/home.png')}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.contain}/>
          </TouchableOpacity>


          <TouchableOpacity 
          onPress={()=> props.navigation.navigate('notifications')}>
          <FastImage 
            source={require('../assets/notification.png')}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.contain}/>
          </TouchableOpacity>
       </View>
      )
};

BottomComponent.defaultProps = {
    handler: () => {},
};


const styles = StyleSheet.create({
    bottomContainer:{
        width:dimensions.SCREEN_WIDTH,
        height:70,
        paddingHorizontal:10,
        paddingVertical:10,
        justifyContent:'space-between',
        alignItems:'center',
        position:'absolute',
        bottom:0,
        left:0,
        right:0,
        backgroundColor:colors.COLOR_PRIMARY,
        flexDirection:'row'
      },
      buttonContainer:{
        width:dimensions.SCREEN_WIDTH * 0.84,
        
        paddingHorizontal:10,
        paddingVertical:10,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        backgroundColor:'white',
        //ios    
        shadowOpacity: 0.3,
        shadowRadius: 3,
        shadowOffset: {
            height: 0,
            width: 0
        },
        //android
        elevation: 2
       
      },
      buttonImage:{
        height:30,
        width:30
      },
      buttonText:{
        fontWeight:'600',
        fontSize:16,
        marginLeft:-40
        
      },
      elevatedButton:{
         //ios    
         shadowOpacity: 0.3,
         shadowRadius: 3,
         shadowOffset: {
             height: 0,
             width: 0
         },
         //android
         elevation: 2,
         backgroundColor:'#094F83',
         height:60,width:60,
         borderRadius:30,
         marginTop:-60,justifyContent:'center',
         alignItems:'center'
      },
  })

export default BottomComponent;
