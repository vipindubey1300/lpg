import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';

import FastImage from 'react-native-fast-image'
import {colors,urls,dimensions} from '../utils/constants';

const CustomTextInput = React.forwardRef((props,ref) =>  {

    const [text , setText ]  = useState('');
    const [isFocus , setFocus ]  = useState(false);

     // The component instance will be extended
     // with whatever you return from the callback passed
     // as the second argument
     useImperativeHandle(ref, () => ({
            getInputValue() {
                return text
            },
            setVal(text){
              setText(text)
            }
      }));


  return (
    <View style={[styles.container, props.style]}>
  
       <TextInput  
        {...props}
        ref={props.inputRef}
        value={text}
        placeholderTextColor={'grey'}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        onChangeText={props.onChangeText ? props.onChangeText : (text) => setText(text)}
        style={styles.inputText}></TextInput>
    
  </View>
 
  );
});

CustomTextInput.defaultProps = {

    focus: () => {},
    style: {},
    placeholder: 'Enter',
    blurOnSubmit: false,
    returnKeyType: 'next',
    keyboardType: null,
    secureTextEntry: false,
    autoCapitalize: "none",
    textContentType: "none",
    defaultValue: '',
    editable: true,
    maxLength:40,
    
};


const styles = StyleSheet.create({
    container: {
     flexDirection:'row',
     alignItems:'center',
     borderColor:'grey',
     borderWidth:1,
     borderRadius:8,
     backgroundColor:colors.WHITE,
     margin:0,
     overflow:'hidden',
     paddingHorizontal:2,
     paddingHorizontal:3,
     marginHorizontal:0,
     marginVertical:10
  
    },
  
    inputText: {
        width:'90%',height:50,textAlign:'center'
    }
  });

export default CustomTextInput;
