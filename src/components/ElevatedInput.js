import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';

import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';


const ElevatedInput = React.forwardRef((props,ref) =>  {

     

    const [text , setText ]  = useState('');
    const [isFocus , setFocus ]  = useState(false);



        // The component instance will be extended
        // with whatever you return from the callback passed
        // as the second argument
        useImperativeHandle(ref, () => ({

            getInputValue() {
                return text
            }

        }));


  return (
    <View style={[styles.container, props.style]}>

  
    <TextInput
      style={[styles.inputText]}
      value={text}
      autoCapitalize={props.autoCapitalize}
      //autoCorrect={false}
      numberOfLines={1}
      maxLength ={props.maxLength}
        onFocus={() => setFocus(true)}
      onBlur={() => setFocus(false)}
      ref={props.inputRef}
      secureTextEntry={props.secureTextEntry}
     //blurOnSubmit={props.blurOnSubmit}
      keyboardType={props.keyboardType}
      returnKeyType={props.returnKeyType}
      placeholder={props.placeholder}
      textContentType={props.textContentType}
      onSubmitEditing={props.onSubmitEditing}
      placeholderTextColor={colors.DARK_GREY}
      onChangeText={props.onChangeText ? props.onChangeText : (text) => setText(text)}
      editable={props.editable}
      multiline={false}
      numberOfLines={1}
      underlineColorAndroid={'rgba(0,0,0,0)'}
      blurOnSubmit={false}
    />
    
  </View>
 
  );
});

ElevatedInput.defaultProps = {

    focus: () => {},
    style: {},
    placeholder: 'Enter',
    blurOnSubmit: false,
    returnKeyType: 'next',
    keyboardType: null,
    secureTextEntry: false,
    autoCapitalize: "none",
    textContentType: "none",
    defaultValue: '',
    editable: true,
    maxLength:40,
};


const styles = StyleSheet.create({
    container: {
    // alignItems:'center',
    
     backgroundColor:colors.WHITE,
     overflow:'hidden',
     paddingHorizontal:2,
     paddingHorizontal:3,
     marginHorizontal:10,
     marginVertical:10,
     //ios    
     shadowOpacity: 0.3,
     shadowRadius: 3,
     shadowOffset: {
         height: 0,
         width: 0
     },
     //android
     elevation: 1
  
    },
  
    inputText: {
      textAlign:'left',
      fontSize: 16,
      flex:8.5,
      color:colors.BLACK,
      marginHorizontal:3
    },
  
  });

export default ElevatedInput;
