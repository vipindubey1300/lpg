import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'


import {colors,urls,dimensions} from '../utils/constants';



const HomeHeader  : () => React$Node = (props) => {


    function _onPress()  {
        props.navigation.toggleDrawer()
      }
      
      return (
       <View style={styles.container}>
            <TouchableOpacity   onPress={_onPress}
            style={{flexDirection:'row',alignItems:'center'}}>
            <FastImage 
            source={require('../assets/menu.png')}
            style={styles.headerImage} 
            resizeMode={FastImage.resizeMode.contain}/>
            </TouchableOpacity>

            {
              props.image 
              ?  <FastImage 
              source={require('../assets/logo.png')}
              style={[styles.headerImage,{marginRight:50,width:150}]} 
              resizeMode={FastImage.resizeMode.contain}/>
              :  <Text style={styles.labelText}>{props.label ? props.label : 'Home'}</Text>
            }

           
            <TouchableOpacity>
            </TouchableOpacity>
    
             
    
       </View>
      )
};

HomeHeader.defaultProps = {
};


const styles = StyleSheet.create({

    container:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      backgroundColor:colors.COLOR_PRIMARY,
      padding:10,
      width:dimensions.SCREEN_WIDTH,
      height:50
    },
    labelText:{
      fontWeight:'400',
      color:colors.WHITE,
      marginRight:dimensions.SCREEN_WIDTH * 0.04,
      fontSize:19,
      marginHorizontal:20
    },
    headerImage:{
        height:20,width:20
    }
  })

export default HomeHeader;
