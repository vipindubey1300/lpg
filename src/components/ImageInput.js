import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput
} from 'react-native';

import FastImage from 'react-native-fast-image'
import {colors,urls,dimensions} from '../utils/constants';

const ImageInput = React.forwardRef((props,ref) =>  {

    const [text , setText ]  = useState('');
    const [isFocus , setFocus ]  = useState(false);

     // The component instance will be extended
     // with whatever you return from the callback passed
     // as the second argument
     useImperativeHandle(ref, () => ({
            getInputValue() {
                return text
            },
            setVal(text){
              setText(text)
            }
      }));


  return (
    <View style={[styles.container, props.style]}>

        <View style={styles.imageContainer}>
        <Image source ={props.inputImage}
                style={styles.image}
                resizeMode={'contain'}
         />
        </View>
  
        <TextInput  
        {...props}
        ref={props.inputRef}
        placeholderTextColor={'grey'}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        onChangeText={ (text) => setText(text)}
        value={ text}
        style={styles.inputText}></TextInput>
    
  </View>
 
  );
});

ImageInput.defaultProps = {

    focus: () => {},
    style: {},
    placeholder: 'Enter',
    blurOnSubmit: false,
    returnKeyType: 'next',
    keyboardType: null,
    secureTextEntry: false,
    autoCapitalize: "none",
    textContentType: "none",
    defaultValue: '',
    editable: true,
    maxLength:40,
    inputImage : require('../assets/user.png')
    
};


const styles = StyleSheet.create({
    container: {
     flexDirection:'row',
     alignItems:'center',
     borderBottomColor:'grey',
     borderBottomWidth:1,
     backgroundColor:colors.WHITE,
     margin:10,
     overflow:'hidden',
     paddingHorizontal:2,
     paddingHorizontal:3,
     marginHorizontal:0,
     marginVertical:10
  
    },
    imageContainer:{
        flex:1.8,
        height:50,justifyContent:'center',
        alignItems:'center'
    },
    image:{
        height:30,width:30
    },
  
    inputText: {
       flex:8,height:50,
    }
  });

export default ImageInput;
