import {Dimensions} from 'react-native';

export const colors = {
  STATUS_BAR_COLOR: "#0C75BD",
  COLOR_PRIMARY: "#0C75BD",
  COLOR_SECONDARY:'#92FF04',
  WHITE:'#FFFFFF',
  BLACK:'#000000',
  LIGHT_MAROON:'#301606',
  DARK_MAROON:'#170B03',
  LIGHT_YELLOW:'#FAFAD2',
  SILVER:' #C0C0C0',
  LIGHT_DARK:'#1e1e1e',
  LIGHT_GREEN:'#7CFC00',
  CRIMSON:'#DC143C',
  GREEN:'#008000',
  MAROON:'#2f0909',
  PALE_GREEN:'#98FB98',
  GREY:'#D3D3D3',
  DARK_GREY:'#C0C0C0',
  RED: "#ec0008",
  PAGE_BACKGROUND:'#EDECEC'

};



export const urls = {
 BASE_URL: 'http://webmobril.org/dev/lpg/api/v1/',
 BASE_URL_MEDIA: 'http://webmobril.org/dev/lpg/',
};


export const dimensions = {
  SCREEN_WIDTH : Dimensions.get('window').width,
  SCREEN_HEIGHT : Dimensions.get('window').height
};



export const values = {
  STRIPE_DEV_KEY :'pk_test_lH3nLCADTo6rIyxvns0P8igB008LHXo6qB',
 
};

