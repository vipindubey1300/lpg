import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity,TextInput
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import ProgressBar from '../components/ProgressBar';

import {colors,urls,dimensions} from '../utils/constants';

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'

import CustomTextInput from '../components/CustomTextInput';


const ITEM = [
    {'name':'Order Placed',id:'1'} ,
    {'name':'Ready To Dispatch',id:'2' }, 
    {'name':'Order Dispatched' ,id:'3'},
    {'name':'Gas Delivered' ,id:'4'}, 
  ]


  const ITEM0 = [
    {'name':'Order Placed',id:'1'} ,
   
  ]

  const ITEM1 = [
    {'name':'Order Placed',id:'1'} ,
    {'name':'Ready To Dispatch',id:'2' }, 
   
  ]

  const ITEM2 = [
    {'name':'Order Placed',id:'1'} ,
    {'name':'Ready To Dispatch',id:'2' }, 
    {'name':'Order Dispatched' ,id:'3'},
   
  ]

  const ITEM3 = [
    {'name':'Order Placed',id:'1'} ,
    {'name':'Ready To Dispatch',id:'2' }, 
    {'name':'Order Dispatched' ,id:'3'},
    {'name':'Gas Delivered' ,id:'4'}, 
  ]
const TrackOrder: () => React$Node = (props) => {

    const [list,setLists] = useState(ITEM);
    const [item,setItem] = useState('')

    useEffect(()=>{
            var item = props.route.params.item
            setItem(item)

            let status = item.status
            if(status == 0) setLists(ITEM0)
            else if(status == 1) setLists(ITEM1)
            else if(status == 2) setLists(ITEM2)
            else if(status == 3) setLists(ITEM3)
    },[])


    const Timeliners = () => {

        
           return (
            list.map((element,index) =>{
                return (
                    <View key={element.id} style={{flexDirection:'row',alignSelf:'flex-start'}}>
                        <View style={{alignItems:'center'}}>
                        <View style={{
                            backgroundColor:colors.COLOR_PRIMARY,
                            height:25,width:25,
                            borderRadius:13,
                        }}/>
    
                       {
                           index != list.length -1 
                           ?
                           <View style={{height:50,width:2,backgroundColor:'grey'}}/>
                           :null
                       }
                        </View>
    
                    <Text style={{margin:7}}>{element.name}</Text>
                    </View>
    
                )
               })
           )

        

    }
   
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>

            <View style={{height:40}}/>


            {/* upper portiona */}

            <TouchableOpacity onPress={()=>props.navigation.navigate('track_order')}
        style={styles.orderContainer}>
            <View style={[styles.orderMain]}>
             <View style={styles.orderLeft}>
                 <Text style={styles.blackText}>Delivery Time</Text>
                 <Text style={styles.blackText}>:</Text>
             </View>
             <View style={styles.orderRight}>
  <Text style={styles.blackText}>{item.delievry_time}</Text>
             </View>
            </View>


            <View style={[styles.orderMain]}>
             <View style={styles.orderLeft}>
                 <Text style={styles.blackText}>Price</Text>
                 <Text style={styles.blackText}>:</Text>
             </View>
             <View style={styles.orderRight}>
  <Text style={styles.blackText}>R {item.price}</Text>
             </View>
            </View>

            <View style={[styles.orderMain]}>
             <View style={styles.orderLeft}>
                 <Text style={styles.blackText}>Quantity</Text>
                 <Text style={styles.blackText}>:</Text>
             </View>
             <View style={styles.orderRight}>
       <Text style={styles.blackText}>{item.qty}</Text>
             </View>
            </View>
       
        </TouchableOpacity>

         {/* upper portiona end */}

         <View style={{height:40}}/>

            <View style={{padding:14,alignSelf:'flex-start'}}>
            <Timeliners/>
            </View>
        

      

      </KeyboardAwareScrollView>
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flexGrow:1
      },
      orderContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9,
        alignSelf:'center',
        backgroundColor:'white',
        borderRadius:8,
        borderColor:'grey',
        borderWidth:1,
        marginHorizontal:8,
        marginVertical:7,
        alignItems:'center',
        overflow:'hidden'
    },
    orderLeft:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',

    },
    orderRight:{
        flex:1,flexDirection:'row',marginHorizontal:10
    },
    orderMain:{
        width:'100%',padding:5,flexDirection:'row',
    },
    whiteText:{
        color:'white',
        fontSize:16
    },
    blackText:{
        color:'black',
        fontSize:16
    }
  
   

})

export default TrackOrder;
