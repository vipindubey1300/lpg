import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import ProgressBar from '../components/ProgressBar';

import {colors,urls,dimensions} from '../utils/constants';

import CustomTextInput from '../components/CustomTextInput';
import ButtonComponent from '../components/ButtonComponent';
import CustomHeader from '../components/CustomHeader';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'
import { showMessage } from '../utils/snackmsg';



const ResetPassword: () => React$Node = (props) => {

    const [loading, setLoading] = useState(false);


// In order to gain access to the child component instance,
  // you need to assign it to a `ref`, so we call `useRef()` to get one
  const emailInput = useRef();
  const emailRef = useRef();
  const passwordInput = useRef();
  const passwordRef = useRef();
  const cnfpasswordInput = useRef();
  const cnfpasswordRef = useRef();


  function isValid(){
    var result = false
    let password = passwordInput.current.getInputValue()
    let cnfpassword = cnfpasswordInput.current.getInputValue()
    
  
    if(password.length == 0) showMessage("Enter password")
    else if(cnfpassword.length == 0) showMessage("Enter confirm password")
    else if(password.length < 8) showMessage("Password should be of 8 characters")
    else if(password != cnfpassword) showMessage("Password and confirm password should match ")
    else result = true
  
    return result
  }
  
  async function _onReset()  {
  
      if(isValid()){
      
        let password = passwordInput.current.getInputValue()
        let cnfpassword = cnfpasswordInput.current.getInputValue()
        var formdata = new FormData()
      
        formdata.append('email',props.route.params.email)
        formdata.append('new_password',password)
        formdata.append('confrim_new_password',cnfpassword)
        console.log("hi1",formdata);         
        setLoading(true)
        fetch(urls.BASE_URL + 'auth/reset_password',
             {method: 'POST', 
              body: formdata}
        )
        .then((response) => response.json())
        .then(async (responseJson) => {
          setLoading(false)
            console.log("hi1",responseJson);             
            if(responseJson.status){
              showMessage('Password Changed Successfully ', false)
              props.navigation.navigate('login')
            }
            else{
              showMessage(responseJson.message)
            }
           
        })
        .catch((error) => {
            setLoading(false)
            showMessage(error.message)
        });
      }
        
       
  }


  useEffect(()=>{
    emailInput.current.setVal(props.route.params.email)
  },[])
  
  

 return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>

        <FastImage 
        source={require('../assets/logo.png')}
        style={styles.logoImage} 
        resizeMode={FastImage.resizeMode.contain}/>


 
        <CustomTextInput
              ref={emailInput}
              placeholder={'Email'}
             // onSubmitEditing={()=> passwordRef.current.focus()}
              inputRef={ emailRef }
              editable={false}
              style={{width:'90%'}}
              returnKeyType="next"
         />
        

            <CustomTextInput
            ref={passwordInput}
            placeholder={'Password'}
           // onSubmitEditing={()=> _onLogin()}
            inputRef={passwordRef}
            style={{width:'90%'}}
            secureTextEntry={true}
            returnKeyType="next"
        />

        <CustomTextInput
        ref={cnfpasswordInput}
        placeholder={'Confirm Password'}
        onSubmitEditing={()=> _onReset()}
        inputRef={cnfpasswordRef}
        secureTextEntry={true}
        style={{width:'90%'}}
        returnKeyType="next"
    />
    

        <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:45,marginBottom:19}}
        handler={_onReset}
        label ={'Save'}/>

     

     




      </KeyboardAwareScrollView>
      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        flexGrow:1,
        backgroundColor:'white'
      },
    logoImage:{
        height:dimensions.SCREEN_HEIGHT * 0.3,
        width:dimensions.SCREEN_WIDTH * 0.45
    }
   

})


export default ResetPassword;
