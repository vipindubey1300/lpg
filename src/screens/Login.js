import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import { showMessage } from '../utils/snackmsg';

import {colors,urls,dimensions} from '../utils/constants';

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'
import { TextInput } from 'react-native-paper';
import CustomTextInput from '../components/CustomTextInput';
import ProgressBar from '../components/ProgressBar';




const Login: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)

    const dispatch = useDispatch()
    const [loading, setLoading] = useState(false);


// In order to gain access to the child component instance,
  // you need to assign it to a `ref`, so we call `useRef()` to get one
  const nameInput = useRef();
  const nameRef = useRef();
  const passwordInput = useRef();
  const passwordRef = useRef();


  async function _onLogi()  {
   
    console.log('on Login FUnction called userReducer---')
    //dispatch()
    await AsyncStorage.setItem("name","Vipin");
    dispatch(addUser({'name':'Vipin'}))
    // props.navigation.navigate('home')

}
  
  function isValid(){
    var result = false
    let name = nameInput.current.getInputValue()
    let password = passwordInput.current.getInputValue()
    
  
    if(name.length == 0) showMessage("Enter username")
    else if(password.length == 0) showMessage("Enter Password")
    else result = true
  
    return result
  }
  
  async function _onLogin()  {
  
      if(isValid()){
      
        console.log('on Login FUnction called userReducer---')
        let name = nameInput.current.getInputValue()
        let password = passwordInput.current.getInputValue()
      
  
        var formdata = new FormData()
      
        formdata.append('username',name)
        formdata.append('password',password)
        formdata.append('device_token','1212121x')
        formdata.append('device_type',Platform.OS == 'android' ? 1 : 2)
        setLoading(true)
        fetch(urls.BASE_URL + 'auth/login',
             {method: 'POST', 
              body: formdata}
        )
        .then((response) => response.json())
        .then(async (responseJson) => {
          setLoading(false)
            console.log("hi1",responseJson);             
            if(responseJson.status){
              showMessage('Login Successfully ', false)
              let obj = {
                'id':responseJson.result.id,
                'email':responseJson.result.email,
                'name':responseJson.result.username,
                'mobile':responseJson.result.mobile,
            } 
            await AsyncStorage.setItem("id",responseJson.result.id.toString());
            await AsyncStorage.setItem("email",responseJson.result.email);
            await AsyncStorage.setItem("name",responseJson.result.username);
            await AsyncStorage.setItem("mobile",responseJson.result.mobile);
                   dispatch(addUser(obj))
            }
            else{
              showMessage(responseJson.message)
            }
           
        })
        .catch((error) => {
            setLoading(false)
            showMessage(error.message)
        });
      }
        
       
  }
 function _onSignUp(){
    props.navigation.navigate('signup')
    //props.navigation.navigate('account')
 }
 function _onForgot(){
    props.navigation.navigate('forgot_password')
 }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>

        <FastImage 
        source={require('../assets/logo.png')}
        style={styles.logoImage} 
        resizeMode={FastImage.resizeMode.contain}/>
       
          <CustomTextInput
          ref={nameInput}
          placeholder={'User name'}
          onSubmitEditing={()=> passwordRef.current.focus()}
          inputRef={ nameRef }
          style={{width:'90%'}}
          returnKeyType="next"
          />
            <CustomTextInput
            ref={passwordInput}
            placeholder={'Password'}
            onSubmitEditing={()=> _onLogin()}
            inputRef={passwordRef}
            style={{width:'90%'}}
            secureTextEntry={true}
            returnKeyType="next"
          />

   
        <TouchableOpacity style={{padding:2,alignSelf:'flex-end'}}
        onPress={()=> _onForgot()}>
      <Text 
        style={styles.forgotText}>Forgot Password?</Text>
      </TouchableOpacity>




    

        <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
        handler={_onLogin}
        label ={'Log In'}/>

        <View style={{flexDirection:'row'}}>
        <Text style={{color:'grey',fontWeight:'500',fontSize:17}}>Don't have an account? </Text>
        <Text onPress={()=> _onSignUp() }
           style={{color:colors.BLACK,textDecorationLine:'underline',fontWeight:'700',fontSize:17}}> Sign Up </Text>
      </View>


        



      </KeyboardAwareScrollView>
      {loading && <ProgressBar/>}

      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
      },
    logoImage:{
        height:dimensions.SCREEN_HEIGHT * 0.3,
        width:dimensions.SCREEN_WIDTH * 0.45,
        marginTop:50
    },
    forgotText:{
        color:'grey',
        textDecorationLine:'underline',
        marginVertical:10,
        fontSize:16
    }
   

})

export default Login;
