import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import ProgressBar from '../components/ProgressBar';

import {colors,urls,dimensions} from '../utils/constants';

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'
import { TextInput } from 'react-native-paper';
import CustomTextInput from '../components/CustomTextInput';
import { showMessage } from '../utils/snackmsg';




const ForgotPassword: () => React$Node = (props) => {
  const [loading, setLoading] = useState(false);

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()
  const emailInput = useRef();
  const emailRef = useRef();



  
 function validEmail(value){
  return  (/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(value))
}


function isValid(){
  var result = false
  let email = emailInput.current.getInputValue()
  
  

  if(email.length == 0) showMessage("Enter email")
  else if(!validEmail(email)) showMessage("Enter valid email")
  else result = true

  return result
}

async function _onForgot()  {
    if(isValid()){
      let email = emailInput.current.getInputValue()
      
      var formdata = new FormData()
    
      formdata.append('mobile_or_email',email)
      console.log(formdata)
      setLoading(true)
      fetch(urls.BASE_URL + 'auth/send_otp',
           {method: 'POST', 
            body: formdata}
      )
      .then((response) => response.json())
      .then(async (responseJson) => {
          console.log("hi1",responseJson);       
          setLoading(false)      
          if(responseJson.status){
              showMessage(responseJson.message,false)
              props.navigation.navigate('otp',{"email" : email,'path':'forgot'});
          }
          else{
            showMessage(responseJson.message)
          }
      })
      .catch((error) => {
          setLoading(false)
          showMessage(error.message)
      });
    }
      
     
}
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>

        <FastImage 
        source={require('../assets/logo.png')}
        style={styles.logoImage} 
        resizeMode={FastImage.resizeMode.contain}/>
       
          <CustomTextInput
          ref={emailInput}
          placeholder={'Email'}
          onSubmitEditing={()=> _onForgot()}
          inputRef={ emailRef }
          style={{width:'90%',marginTop:30}}
          keyboardType={'email-address'}
          returnKeyType="go"
          />
            
   


    

        <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
        handler={_onForgot}
        label ={'Next'}/>

      

      </KeyboardAwareScrollView>
      {loading && <ProgressBar/>}

      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:'white',
        flexGrow:1
      },
    logoImage:{
        height:dimensions.SCREEN_HEIGHT * 0.3,
        width:dimensions.SCREEN_WIDTH * 0.45,
        marginTop:50
    },
    forgotText:{
        color:'grey',
        textDecorationLine:'underline',
        marginVertical:10,
        fontSize:16
    }
   

})

export default ForgotPassword;
