import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity,TextInput
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'

import CustomTextInput from '../components/CustomTextInput';
import ImageInput from '../components/ImageInput';
import { showMessage } from '../utils/snackmsg';




const Account: () => React$Node = (props) => {

    const emailInput = useRef();
    const emailRef = useRef();
    const passwordInput = useRef();
    const passwordRef = useRef();
    const nameInput = useRef();
    const nameRef = useRef();
    const phoneInput = useRef();
    const phoneRef = useRef();



    const [username, setUsername ] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');
    const [loading, setLoading] = useState(false);
    const userReducer = useSelector(state => state.userReducer)
     const dispatch = useDispatch()


 function getProfile(){
       
      var formdata = new FormData()
      formdata.append('user_id',userReducer.user.id)
      setLoading(true)
      fetch(urls.BASE_URL + 'auth/get_profile',
           {method: 'POST',body:formdata }
      )
      .then((response) => response.json())
      .then(async (responseJson) => {
        setLoading(false)
          console.log("hi1",responseJson);             
          if(responseJson.status){
           

            phoneInput.current.setVal(responseJson.result.mobile)
            emailInput.current.setVal(responseJson.result.email)
           // nameInput.current.setVal(responseJson.result.username)
          }
          else{
            showMessage(responseJson.message)
          }
         
      })
      .catch((error) => {
          setLoading(false)
          showMessage(error.message)
      });
    }


     useEffect(()=>{
       getProfile()
       
       // setPhone(userReducer.user.mobile)
         phoneInput.current.setVal(userReducer.user.mobile)
         emailInput.current.setVal(userReducer.user.email)
         nameInput.current.setVal(userReducer.user.name)
     },[])



     function validEmail(value){
      return  (/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(value))
    }
    
    function isValid(){
      
      var result = false
      let email = emailInput.current.getInputValue()
      let name = nameInput.current.getInputValue()
      let phone = phoneInput.current.getInputValue()
    
    
      if(name.length == 0) showMessage("Enter username ");
      else if(phone.length == 0) showMessage("Enter Phone Number")
      else if(phone.length < 9 ) showMessage("Enter valid  Phone Number")
      else if(email.length == 0) showMessage("Enter email")
      else if(!validEmail(email)) showMessage("Enter valid email")
      else result = true
    
      return result
    }
    
    async function _onEdit()  {
        if(isValid()){
        
          
          let phone = phoneInput.current.getInputValue()
    
          var formdata = new FormData()
          // formdata.append('username',name)
          // formdata.append('email',email)
          formdata.append('mobile',phone)
         
          setLoading(true)
          formdata.append('user_id',userReducer.user.id)
          fetch(urls.BASE_URL + 'auth/edit_profile',
               {method: 'POST', 
                body: formdata}
          )
          .then((response) => response.json())
          .then(async (responseJson) => {
              console.log("hi1",responseJson);  
              setLoading(false)           
              if(responseJson.status){
                  // let obj = {
                  //     'id':responseJson.result.id,
                  //     'email':responseJson.result.email,
                  //     'name':responseJson.result.username,
                  //     'mobile':responseJson.result.mobile,
                  // } 
                  // await AsyncStorage.setItem("id",responseJson.result.id.toString());
                  // await AsyncStorage.setItem("email",responseJson.result.email);
                  // await AsyncStorage.setItem("name",responseJson.result.username);
                  // await AsyncStorage.setItem("mobile",responseJson.result.mobile);
                  // dispatch(addUser(obj))
                  props.navigation.navigate('home')
                }
              else{
                
                showMessage(responseJson.message)
              }
             
          })
          .catch((error) => {
            setLoading(false)
            showMessage(error.message)
          });
         
        }
          
         
    }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>

    <View style={{height:50}}/>


<View style={styles.containerAccount}>


        <TouchableOpacity>
                <FastImage 
                source={require('../assets/user-profile.png')}
                style={{height:120,width:120,borderRadius:60,overflow:'hidden',margin:10}} 
                resizeMode={FastImage.resizeMode.cover}/>
            </TouchableOpacity>

         <View style={styles.divider}/>


            <ImageInput
            
             ref={nameInput}
              placeholder={'Name'}
              inputImage = {require('../assets/user.png')}
              onSubmitEditing={()=> phoneRef.current.focus()}
              inputRef={ nameRef }
              style={{width:'90%'}}
              editable={false}
              returnKeyType="next"
            />


        <ImageInput
          //  value ={phone}
            ref={phoneInput}
            placeholder={'Phone Number'}
            keyboardType="numeric"
            inputImage = {require('../assets/phone.png')}
            textContentType='telephoneNumber'
            onSubmitEditing={()=>  emailRef.current.focus()}
            inputRef={phoneRef}
            style={{width:'90%'}}
            returnKeyType="next"
        />
        <ImageInput
       
              ref={emailInput}
              placeholder={'Email ID'}
              inputImage = {require('../assets/mail.png')}
             // onSubmitEditing={()=> passwordRef.current.focus()}
             editable={false}
              inputRef={ emailRef }
              style={{width:'90%'}}
              returnKeyType="next"
         />
        


    

    </View>
            

    

        <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
        handler={_onEdit}
        label ={'Update '}/>

      

      </KeyboardAwareScrollView>
      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flexGrow:1
      },
    input:{
        height:45,
        width:'95%',
        alignSelf:'center',
        borderRadius:10,
        borderWidth:0.8,
        borderColor:'black',
        padding:8,
        marginVertical:15,
        marginHorizontal:4,
        backgroundColor:'white',
        
    },
    containerAccount:{
        backgroundColor:'white',
        borderRadius:8,
        borderWidth:1,
        borderColor:'grey',
        alignItems:'center',
        width:'100%'
    },
    divider:{
        height:2,
        width:'100%',
        backgroundColor:'grey',
        marginVertical:10
    }
   

})

export default Account;
