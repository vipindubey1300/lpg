import React, { useState ,forwardRef, useRef, useImperativeHandle,useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TextInput,Keyboard
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import ProgressBar from '../components/ProgressBar';

import {colors,urls,dimensions} from '../utils/constants';

import CustomTextInput from '../components/CustomTextInput';
import ButtonComponent from '../components/ButtonComponent';
import CustomHeader from '../components/CustomHeader';

import { showMessage } from '../utils/snackmsg';

import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'



const Otp = (props) => {
    const [loading, setLoading] = useState(false);


  const [otp1,setOtp1] = useState("");
  const [otp2,setOtp2] = useState("");
  const [otp3,setOtp3] = useState("");
  const [otp4,setOtp4] = useState("");
  const [otp5,setOtp5] = useState("");
  const [otp6,setOtp6] = useState("");

  const  otp1Ref =  useRef();
  const otp2Ref = useRef();
  const otp3Ref = useRef();
  const otp4Ref = useRef();
  const otp5Ref = useRef();
  const otp6Ref = useRef();


    // const userReducer = useSelector(state => state.userReducer)

    // const dispatch = useDispatch()


    function isValid(){
        var result = false
        let otp = otp1+otp2+otp3+otp4+otp5+otp6
       
        if(otp.length != 6) showMessage("Enter valid OTP")
        else result = true
      
        return result
      }
      
     function _onOtp()  {
        let otp = otp1+otp2+otp3+otp4+otp5+otp6
            console.log("hi1-------",otp); 
            if(isValid()){
  
            var formdata = new FormData()
            formdata.append('email',props.route.params.email)
            formdata.append('otp',otp)
            formdata.append('from_forgotpassword_screen',1)
           
            console.log("hi1",formdata);  
            setLoading(true)
            fetch(urls.BASE_URL + 'auth/verify_otp',
                 {method: 'POST', 
                  body: formdata}
            )
            .then((response) => response.json())
            .then(async (responseJson) => {
                setLoading(false)  
                console.log("hi1",responseJson);           
                if(responseJson.status){
                    showMessage(responseJson.message,true)
                    props.navigation.navigate('reset_password',{'email':props.route.params.email})
                }
                else{
                  showMessage(responseJson.message)
                }
            })
            .catch((error) => {
              setLoading(false)   
              showMessage(error.message)
            });
          
            
            }
           
      }

    
    

    useEffect(() => {
      console.log("Component Did Mount")
      // returned function will be called on component unmount 
      return () => {
        console.log("Component Will UnMount")
      }
      //Notice the second parameter here (empty array). This will run only once.
     }, [])

 return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        automaticallyAdjustContentInsets={ true }
       // keyboardDismissMode="on-drag"
        keyboardShouldPersistTaps={'always'} 
       
          
        contentContainerStyle={styles.scrollContainer}>

        <FastImage 
        source={require('../assets/logo.png')}
        style={styles.logoImage} 
        resizeMode={FastImage.resizeMode.contain}/>

        <Text style={styles.subHeadingText}>Enter 6 - digit OTP sent to your email address.</Text>
        
      
        
        <View style={styles.otpView}>

<View style={styles.otpTextInputView}>
    <TextInput
        ref={otp1Ref}
        style={styles.textInput}
        value={otp1}
        maxLength={1}
        //editable={false}
        onKeyPress={e =>{ 
            if(e.nativeEvent.key == 'Backspace'){
               // otp1Ref.current.focus();
        }else{
            console.log("dhfgfj")
            otp2Ref.current.focus()
        }}}
        onChangeText={(value)=>{setOtp1(value); 
            if(value !== ""){
                otp2Ref.current.focus() 
            }
         
        }}
        keyboardType="phone-pad" 
        />
</View>
<View style={styles.otpTextInputView}>
    <TextInput
      ref={otp2Ref}
        style={styles.textInput}
        maxLength={1}
        value={otp2}
        //editable={false}
        onChangeText={(value)=>{setOtp2(value); 
            if(value !== ""){
                otp3Ref.current.focus() 
            }
           }} 
        onKeyPress={e =>{ 
            if(e.nativeEvent.key == 'Backspace'){
                otp1Ref.current.focus();
                setOtp1("");
                return;
            }else{
                otp3Ref.current.focus()
            }}} 
            keyboardType="phone-pad" />
</View>
<View style={styles.otpTextInputView}>
     <TextInput
       ref={otp3Ref}
        style={styles.textInput}
        maxLength={1}
        value={otp3}
        //editable={false}
        onChangeText={(value)=>{setOtp3(value); 
            if(value !== ""){
                otp4Ref.current.focus();
            }
        }}  
        onKeyPress={e =>{ 
            if(e.nativeEvent.key == 'Backspace'){
                otp2Ref.current.focus();
                setOtp2("");
                return;
        }else{
            otp4Ref.current.focus()
        }}} 
        keyboardType="phone-pad" />
</View>
<View style={styles.otpTextInputView}>
     <TextInput
       ref={otp4Ref}
        style={styles.textInput}
        value={otp4}
        maxLength={1}
        //editable={false}
        onKeyPress={e =>{ 
            if(e.nativeEvent.key == 'Backspace'){
                otp3Ref.current.focus();
                setOtp3("");
                return;
        }else{

        }}}
        onChangeText={(value)=>{setOtp4(value); 
            if(value !== ""){
                otp5Ref.current.focus();
            }
        }}
        keyboardType="phone-pad" />
</View>
<View style={styles.otpTextInputView}>
     <TextInput
       ref={otp5Ref}
        style={styles.textInput}
        value={otp5}
        maxLength={1}
        //editable={false}
        onKeyPress={e =>{ 
            if(e.nativeEvent.key == 'Backspace'){
                otp4Ref.current.focus();
                setOtp4("");
                return;
        }else{

        }}}
        onChangeText={(value)=>{setOtp5(value); 
            if(value !== ""){
                otp6Ref.current.focus();
            }
        }}
        keyboardType="phone-pad" />
</View>

<View style={styles.otpTextInputView}>
     <TextInput
       ref={otp6Ref}
        style={styles.textInput}
        value={otp6}
        maxLength={1}
        //editable={false}
        onKeyPress={e =>{ 
            if(e.nativeEvent.key == 'Backspace'){
                otp5Ref.current.focus();
                setOtp5("");
                return;
        }else{

        }}}
        onChangeText={(value)=>{setOtp6(value); 
        }}
        keyboardType="phone-pad" />
</View>

</View>

    

          <ButtonComponent 
          style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:75,marginBottom:19}}
          handler={_onOtp}
          label ={'Next'}/>



      </KeyboardAwareScrollView>
      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        flexGrow:1,
        backgroundColor:'white'
       
      },
    logoImage:{
        height:dimensions.SCREEN_HEIGHT * 0.3,
        width:dimensions.SCREEN_WIDTH * 0.45
    },
    headingText:{
        fontSize:19,
        fontWeight:'bold',margin:6,marginTop:-20
    },
    subHeadingText:{
      fontWeight:'bold',margin:6
    },
    input: {
        height: 45,
        padding: 7,
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 18,
        borderBottomWidth: 1,
        borderRadius: 10,
        borderBottomColor: '#48BBEC',
        textAlign:'center',
        width:'95%',
    },
    textInput:{
      alignSelf:"center",fontSize:14,color:"black",padding:Platform.OS  == "ios" ? 10 : 0,
      textAlign:"center"
  },
  otpView:{
      width:"100%",
      flexDirection:"row",
      justifyContent:"space-evenly",
      alignItems:"center",
      marginTop:40,
  },
  otpTextInputView:{
      borderWidth:1.5,
      borderColor:"black",
      height:35,
      width:35,
      justifyContent:"center",
      alignItems:"center"
  },
   

})


export default Otp;
