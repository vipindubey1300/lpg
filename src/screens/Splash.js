import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';


const Splash: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor={colors.STATUS_BAR_COLOR}/>
      <SafeAreaView style={styles.container}>

       <FastImage 
        source={require('../assets/logo.png')}
        style={styles.splash_image} 
        resizeMode={FastImage.resizeMode.contain}/>
      
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      justifyContent:'center',
      alignItems:'center',
      backgroundColor:'white'
    },
    splash_image:{
      height: dimensions.SCREEN_HEIGHT * 0.22, 
      width:dimensions.SCREEN_WIDTH * 0.43, 
    },
})

export default Splash;
