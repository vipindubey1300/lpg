import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,Image,TouchableOpacity,TextInput, Platform
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import MapView, { Marker } from 'react-native-maps';

import {colors,urls,dimensions} from '../utils/constants';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoder';
import RBSheet from "react-native-raw-bottom-sheet";

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'

import CustomTextInput from '../components/CustomTextInput';
import { showMessage } from '../utils/snackmsg';
import ImageInput from '../components/ImageInput';




const Location: () => React$Node = (props) => {


  const streetInput = useRef();
  const streetRef = useRef();
  const localityInput = useRef();
  const localityRef = useRef();
  const cityInput = useRef();
  const cityRef = useRef();
  const countryInput = useRef();
  const countryRef = useRef();
  const _map = useRef(null);
  const refRBSheet = useRef();

    const dispatch = useDispatch()

    const [region, setRegion] = useState({
      latitude: 51.5079145,
      longitude: -0.0899163,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01
    });

    const [address, setAddress] = useState('');
    const [modalVisibile, setModalVisibile] = useState(false);
    
    
   

    function _onNext(){
      let street = streetInput.current.getInputValue()
      let locality = localityInput.current.getInputValue()
      let city = cityInput.current.getInputValue()
      let country = countryInput.current.getInputValue()


      var fulladdres = street + ',' + locality + ',' +city + ',' + country
      if(fulladdres.length == 0) showMessage("Enter location first !")
      else props.navigation.navigate('booking',{'location':fulladdres})
    }

    function _onDone(){
      let street = streetInput.current.getInputValue()
      let locality = localityInput.current.getInputValue()
      let city = cityInput.current.getInputValue()
      let country = countryInput.current.getInputValue()

      if(street.length == 0) showMessage("Enter Street")
      else if(locality.length == 0) showMessage("Enter locality")
      else if(city.length == 0) showMessage("Enter city")
      else if(country.length == 0) showMessage("Enter country")
      else{
        var fulladdres = street + ',' + locality + ',' +city + ',' + country
       
        setAddress(fulladdres)
         
        streetInput.current.setVal(street)
        cityInput.current.setVal(city)
        localityInput.current.setVal(locality)
        countryInput.current.setVal(country)
        
        //refRBSheet.current.close()
      }


     
    }


    function  getaddress(lat,long){
            var NY = { lat:lat, lng: long };
            Geocoder.geocodePosition(NY).then(res => {
              console.log(res)
              var addr  = res[0].formattedAddress
              var streetName = res[0].feature
              var locality = res[0].subLocality
              var city = res[0].locality
              var country = res[0].country

              var fulladdres = streetName + ',' + locality + ',' +city + ',' + country
               setAddress(fulladdres)

               streetInput.current.setVal(streetName)
               cityInput.current.setVal(city)
               localityInput.current.setVal(locality)
               countryInput.current.setVal(country)

            })
            .catch(err =>{
             // ToastAndroid.show("Cannot get this location !",ToastAndroid.SHORT);
              Platform.OS === 'android' 
              ? ToastAndroid.show("Cannot get this location !", ToastAndroid.SHORT)
              : Alert.alert(err.message)
            })
}



   function getCurrentLocation (){
    Geolocation.getCurrentPosition(address => {
      console.log(address)
      var lat = address.coords.latitude
      var lng = address.coords.longitude
      getaddress(lat,lng)
      

      setRegion({
        latitude: lat,
        longitude:lng,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      })


      if(_map.current) {
        _map.current.animateCamera(
          {
            center: {
              latitude:lat,
              longitude:lng
            },
            zoom: 15
          },
          3000
        );
      }
    });
   }

   useEffect (() =>{
       getCurrentLocation()
    },[])

    

   /** 

    const AddrSheet = (props) =>{
      return( 
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        //closeOnPressMask={false}
        height={dimensions.SCREEN_HEIGHT * 0.6}
        customStyles={{
          container: {
            justifyContent: "center",
            alignItems: "center"
          },
          wrapper: {
            backgroundColor: "transparent",
           
          },
          draggableIcon: {
            backgroundColor: "#000"
          }
        }}
      >

        <CustomTextInput
          ref={streetInput}
          placeholder={'Street'}
          onSubmitEditing={()=> localityRef.current.focus()}
          inputRef={ streetRef }
          style={{width:'90%'}}
          returnKeyType="next"
          />
         <CustomTextInput
            ref={localityInput}
            placeholder={'Locality'}
            onSubmitEditing={()=> cityRef.current.focus()}
            inputRef={localityRef}
            style={{width:'90%'}}
            secureTextEntry={true}
            returnKeyType="next"
          />  

           <CustomTextInput
            ref={cityInput}
            placeholder={'City'}
            onSubmitEditing={()=> countryRef.current.focus()}
            inputRef={ cityRef }
            style={{width:'90%'}}
            returnKeyType="next"
          />
         <CustomTextInput
            ref={countryInput}
            placeholder={'Country'}
            onSubmitEditing={()=> _onDone()}
            inputRef={countryRef}
            style={{width:'90%'}}
            secureTextEntry={true}
            returnKeyType="next"
          />   
   

      <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
        handler={_onDone}
        label ={'Done'}/>

     
      </RBSheet>
)
    }
*/
    const AddrView = (props) =>{
      return( 
        <View>

        <CustomTextInput
          ref={streetInput}
          placeholder={'Street'}
          onSubmitEditing={()=> localityRef.current.focus()}
          inputRef={ streetRef }
          style={{width:'90%'}}
          returnKeyType="next"
          />
         <CustomTextInput
            ref={localityInput}
            placeholder={'Locality'}
            onSubmitEditing={()=> cityRef.current.focus()}
            inputRef={localityRef}
            style={{width:'90%'}}
            returnKeyType="next"
          />  

           <CustomTextInput
            ref={cityInput}
            placeholder={'City'}
            onSubmitEditing={()=> countryRef.current.focus()}
            inputRef={ cityRef }
            style={{width:'90%'}}
            returnKeyType="next"
          />
         <CustomTextInput
            ref={countryInput}
            placeholder={'Country'}
            onSubmitEditing={()=> _onDone()}
            inputRef={countryRef}
            style={{width:'90%'}}
            returnKeyType="next"
          />   
   

              <ButtonComponent 
              style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
              handler={_onDone}
              label ={'Done'}/>

     
         </View>)
    }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
       
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>

            <Text style={{color:'green',fontSize:20,marginVertical:10,
            alignSelf:'flex-start',marginHorizontal:10,fontWeight:'500'}}>Please Enter location</Text>

            <TouchableOpacity  onPress={() => {setModalVisibile(!modalVisibile)}} 
            style={styles.addressContainer}>
              <Text>{address}</Text>
            </TouchableOpacity>
            <View style={{
                height:dimensions.SCREEN_HEIGHT * 0.3,
                width:dimensions.SCREEN_WIDTH * 0.9,
                borderRadius:15,
                overflow:'hidden'
            }}>

                <MapView
                    style={{height:'100%',width:'100%'}}
                    region={region}
                    showsMyLocationButton={true}
                    showsUserLocation = {true}
                    followUserLocation = {true}
                    zoomEnabled = {true}
                    onRegionChangeComplete={region => setRegion(region)} >
                 <Marker 

                    coordinate={{ 
                        latitude: region.latitude, longitude:region.longitude }}
                       draggable
                       title={"Move it"}
                      onDragEnd={(e) => {
                        // this.setState({latitude:e.nativeEvent.coordinate.latitude,longitude:e.nativeEvent.coordinate.longitude} )
                        getaddress(e.nativeEvent.coordinate.latitude,e.nativeEvent.coordinate.longitude)

                      }} />
                  </MapView>
            </View>

                {
                  modalVisibile && <AddrView />
                }
           

    

        <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
        handler={_onNext}
        label ={'Next'}/>

      

      </KeyboardAwareScrollView>
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flexGrow:1
      },
      addressContainer:{
        width:dimensions.SCREEN_WIDTH * 0.88,
        borderWidth:1,
        borderRadius:10,
        borderColor:'black',
        padding:10,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        margin:15,
        backgroundColor:'#eee',
        height:45
      },
      input:{
        width:'90%',
        height:45,
        borderRadius:10,
        borderColor:'black',
        borderWidth:0.7,
        padding:4,
        alignSelf:'center',
        margin:10
      }
   

})

export default Location;
