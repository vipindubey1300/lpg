import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';

import ButtonComponent from '../components/ButtonComponent';
import CustomHeader from '../components/CustomHeader';
import { showMessage } from '../utils/snackmsg';

import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'
import CustomTextInput from '../components/CustomTextInput';
import ProgressBar from '../components/ProgressBar';
import { CheckBox } from 'react-native-elements'; // 0.16.0


const SignUp: () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()

    const [loading, setLoading] = useState(false);
    const [checked, setChecked] = useState(false);


  const emailInput = useRef();
  const emailRef = useRef();
  const passwordInput = useRef();
  const passwordRef = useRef();
  const nameInput = useRef();
  const nameRef = useRef();
  const phoneInput = useRef();
  const phoneRef = useRef();

 function validEmail(value){
  return  (/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(value))
}

function isValid(){
  
  var result = false
  let email = emailInput.current.getInputValue()
  let name = nameInput.current.getInputValue()
  let password = passwordInput.current.getInputValue()
  let phone = phoneInput.current.getInputValue()


  if(name.length == 0) showMessage("Enter username ");
  else if(phone.length == 0) showMessage("Enter Phone Number")
  else if(phone.length < 9 ) showMessage("Enter valid  Phone Number")
  else if(email.length == 0) showMessage("Enter email")
  else if(!validEmail(email)) showMessage("Enter valid email")
  else if(password.length == 0) showMessage("Enter Password")
  else if(password.length < 8) showMessage("Password should be 8 characters long")
  else if(!checked) showMessage("Accept terms and conditions first")
  else result = true

  return result
}

async function _onSignUp()  {
    if(isValid()){
    
      let email = emailInput.current.getInputValue()
      let name = nameInput.current.getInputValue()
      let password = passwordInput.current.getInputValue()
      let phone = phoneInput.current.getInputValue()

      var formdata = new FormData()
      formdata.append('username',name)
      formdata.append('email',email)
      formdata.append('password',password)
      formdata.append('mobile',phone)
      formdata.append('is_term_accept',1)
      formdata.append('device_token','1212121x')
      formdata.append('device_type',Platform.OS == 'android' ? 1 : 2)
      setLoading(true)
      fetch(urls.BASE_URL + 'auth/register',
           {method: 'POST', 
            body: formdata}
      )
      .then((response) => response.json())
      .then(async (responseJson) => {
          console.log("hi1",responseJson);  
          setLoading(false)           
          if(responseJson.status){
            showMessage('Registered Successfully ', false)
              let obj = {
                  'id':responseJson.result.id,
                  'email':responseJson.result.email,
                  'name':responseJson.result.username,
                  'mobile':responseJson.result.mobile,
              } 
              await AsyncStorage.setItem("id",responseJson.result.id.toString());
              await AsyncStorage.setItem("email",responseJson.result.email);
              await AsyncStorage.setItem("name",responseJson.result.username);
              await AsyncStorage.setItem("mobile",responseJson.result.mobile);
              dispatch(addUser(obj))          }
          else{
            
            showMessage(responseJson.message)
          }
         
      })
      .catch((error) => {
        setLoading(false)
        showMessage(error.message)
      });
     
    }
      
     
}


 return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>

        <FastImage 
        source={require('../assets/logo.png')}
        style={styles.logoImage} 
        resizeMode={FastImage.resizeMode.contain}/>


         <CustomTextInput
              ref={nameInput}
              placeholder={'UserName'}
              onSubmitEditing={()=> phoneRef.current.focus()}
              inputRef={ nameRef }
              style={{width:'90%',marginTop:-20}}
              returnKeyType="next"
         />
        

            <CustomTextInput
            ref={phoneInput}
            placeholder={'Phone Number'}
            keyboardType="numeric"
          
              textContentType='telephoneNumber'
            onSubmitEditing={()=>  emailRef.current.focus()}
            inputRef={phoneRef}
            style={{width:'90%'}}
            returnKeyType="next"
        />
        <CustomTextInput
              ref={emailInput}
              placeholder={'Email ID'}
              keyboardType={'email-address'}
              onSubmitEditing={()=> passwordRef.current.focus()}
              inputRef={ emailRef }
              style={{width:'90%'}}
              returnKeyType="next"
         />
        

            <CustomTextInput
            ref={passwordInput}
            placeholder={'Password'}
            secureTextEntry={true}
            onSubmitEditing={()=> _onSignUp()}
            inputRef={passwordRef}
            style={{width:'90%'}}
            returnKeyType="next"
        />

        <View style={{flexDirection:'row',alignItems:'center',marginTop:10}}>
        <CheckBox
            
            checked={checked}
            onPress={() => {
              setChecked(!checked);
            }}
         />
         <Text onPress={()=> props.navigation.navigate('terms')}>  Accept all terms and conditions</Text>
        </View>
    

        <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
        handler={_onSignUp}
        label ={'Sign Up'}/>


        <View style={{flexDirection:'row'}}>
              <Text style={{color:'grey',fontWeight:'500',fontSize:17}}>Already have an account? </Text>
              <Text onPress={()=> props.navigation.goBack() }
                 style={{color:colors.BLACK,textDecorationLine:'underline',fontWeight:'700',fontSize:17}}> Sign In </Text>
         </View>
                         

     




      </KeyboardAwareScrollView>

      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:'white',
        flexGrow:1
      },
    logoImage:{
        height:dimensions.SCREEN_HEIGHT * 0.3,
        width:dimensions.SCREEN_WIDTH * 0.48
    }
   

})


export default SignUp;


