import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image, TouchableOpacity
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';
import Carousel from 'react-native-banner-carousel';

import CustomTextInput from '../components/CustomTextInput';
import ButtonComponent from '../components/ButtonComponent';
import HomeHeader from '../components/HomeHeader';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'
import BottomComponent from '../components/BottomComponent';

const images = [
  require('../assets/banner3.jpg'),
  require('../assets/banner2.jpg'),
  require('../assets/banner4.jpg'),
 
];

const Home : () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)

    const dispatch = useDispatch()

    
    function getView(index){
      if(index == 0) return '../assets/banner1.jpg'
      else if(index == 1) return '../assets/banner2.jpg'
      else if(index == 2) return '../assets/banner3.jpg'
    }
    function  renderPage(image, index) {
      console.log(image)
      return (
          <View style={{backgroundColor:'white'}} key={index}>
              <Image style={{ width: '100%', height: '100%'}}  source={image}  resizeMode= { index == 0 ?'contain' :'cover'} />
          </View>
      );
    }

    function _washing(){
      props.navigation.navigate('washing')
    }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
      <HomeHeader image={true} {...props}/>
        <KeyboardAwareScrollView
        automaticallyAdjustContentInsets={ true }
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.scrollContainer}>

        <View style={styles.bannerContainer}>
        <Carousel
          autoplay
          autoplayTimeout={5000}
          loop
          index={0}
          activePageIndicatorStyle={{backgroundColor:'black'}}
          pageSize={dimensions.SCREEN_WIDTH}
        >
           {images.map((image, index) => renderPage(image, index))}
    </Carousel>
        </View>

        <View style={{height:40}}/>


        {/**  between */}

        <TouchableOpacity onPress={()=> props.navigation.navigate('location')}
         style={styles.betweenContainer}>
          <Text style={styles.betweenText}>LPG Selection</Text>
          <FastImage 
            source={require('../assets/quality.png')}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.contain}/>
        </TouchableOpacity>


        <TouchableOpacity style={styles.betweenContainer}>
          <Text style={styles.betweenText}>Please Select Your Location</Text>
          <FastImage 
            source={require('../assets/quality.png')}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.contain}/>
        </TouchableOpacity>




        <TouchableOpacity style={styles.betweenContainer}>
          <Text style={styles.betweenText}>Book Your Order</Text>
          <FastImage 
            source={require('../assets/quality.png')}
            style={styles.buttonImage} 
            resizeMode={FastImage.resizeMode.contain}/>
        </TouchableOpacity>

          {/**  between end */}

      
       
       {/**  bottom */}

        <BottomComponent {...props}/>

       {/**  bottom  end */}



      </KeyboardAwareScrollView>
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    scrollContainer:{
        padding:0,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flexGrow:1
      },
    bannerContainer:{
      width:dimensions.SCREEN_WIDTH,
      height:dimensions.SCREEN_HEIGHT *0.35,
     
    },
    
    betweenContainer:{
      width:dimensions.SCREEN_WIDTH * 0.88,
      borderWidth:1,
      borderRadius:10,
      borderColor:'black',
      padding:10,
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      margin:10,
      backgroundColor:'white'
    },
    betweenText:{
      color:'black',
      fontWeight:'500',
      fontSize:16
    },
    buttonImage:{
      height:30,
      width:30
    },
   

})

export default Home;
