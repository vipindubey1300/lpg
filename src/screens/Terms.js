import React, { useState ,useEffect, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity,TextInput
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import ProgressBar from '../components/ProgressBar';

import {colors,urls,dimensions} from '../utils/constants';
import HTML from 'react-native-render-html';

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'

import CustomTextInput from '../components/CustomTextInput';
import { showMessage } from '../utils/snackmsg';




const Terms: () => React$Node = (props) => {


  const [loading, setLoading] = useState(false);
  const [html, setHtml] = useState(false);
  const userReducer = useSelector(state => state.userReducer)
   const dispatch = useDispatch()


   function getTerms(){
  
    setLoading(true)
    fetch(urls.BASE_URL + 'auth/terms_and_conditions',
         {method: 'GET' }
    )
    .then((response) => response.json())
    .then(async (responseJson) => {
      setLoading(false)
        console.log("hi1",responseJson);             
        if(responseJson){
          setHtml(responseJson.description)
        }
    })
    .catch((error) => {
        setLoading(false)
        showMessage(error.message)
    });
  }


  useEffect(() =>{
    getTerms()
  },[])



  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>

         <HTML html={html}  />
         
      </KeyboardAwareScrollView>
      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:15,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flexGrow:1
      },
   
   

})

export default Terms;
