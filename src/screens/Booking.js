import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity,TextInput
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import { Container, Content, Picker, Form, Icon } from "native-base";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DatePicker from 'react-native-datepicker'

import {colors,urls,dimensions} from '../utils/constants';
import { RadioButton } from 'react-native-paper';
import ProgressBar from '../components/ProgressBar';

import ButtonComponent from '../components/ButtonComponent';
import { showMessage } from '../utils/snackmsg';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'





const Booking: () => React$Node = (props) => {

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const [fuel, setFuel] = useState('');
    const [quantity, setQuantity] = useState('1');
    const [price, setPrice] = useState(0);

    const [fuelTwo, setFuelTwo] = useState('');
    const [quantityTwo, setQuantityTwo] = useState('1');
    const [priceTwo, setPriceTwo] = useState(0);

    const [fuelThree, setFuelThree] = useState('');
    const [quantityThree, setQuantityThree] = useState('1');
    const [priceThree, setPriceThree] = useState(0);


    const [fuelFour, setFuelFour] = useState('');
    const [quantityFour, setQuantityFour] = useState('1');
    const [priceFour, setPriceFour] = useState(0);

    const [fuelFive, setFuelFive] = useState('');
    const [quantityFive, setQuantityFive] = useState('1');
    const [priceFive, setPriceFive] = useState(0);

    const [addMoreStatusTwo, setAddMoreStatusTwo] = useState(false);
    const [addMoreStatusThree, setAddMoreStatusThree] = useState(false);
    const [addMoreStatusFour, setAddMoreStatusFour] = useState(false);
    const [addMoreStatusFive, setAddMoreStatusFive] = useState(false);


    const [bookDate, setBookDate] = useState('');
    const [time, setTime] = React.useState('morning');
    
    const [loading, setLoading] = useState(false);
    const userReducer = useSelector(state => state.userReducer)
     const dispatch = useDispatch()


    const [gases,setGases] = useState([])


  function  convertDate(date) {
      var yyyy = date.getFullYear().toString();
      var mm = (date.getMonth()+1).toString();
      var dd  = date.getDate().toString();
  
      var mmChars = mm.split('');
      var ddChars = dd.split('');
  
      return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
    }

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };
     


    const handleConfirm = (date) => {
        console.warn("A date has been picked: ", convertDate(date));
        setBookDate(convertDate(date))
        hideDatePicker();
    };


    const AddMoreTwo = () => {
        return(
            <View>
                 {/** fuel */}
                 <View  style={styles.dropdownStyle}>
             <Picker
             selectedValue={fuelTwo}
             mode="dropdown"
             placeholder="LPG Gas (other)"
             placeholderStyle={{ color: "grey" }}
             placeholderIconColor="#007aff"
             iosIcon={<Icon name="caret-down" />}
             onValueChange={(itemValue, itemIndex) => {
              let obj = gases.find(o => o.id == itemValue);
              setPriceTwo(obj.price)
              setFuelTwo(itemValue)
             }
               }         
           >
                {
                gases.map(e =>{
                 return <Picker.Item label={e.name} value={e.id} key={e.price}/>
                })
              }
             </Picker>
          </View>
         {/** fuel  End*/}



   {/** quanityt */}
   <View  style={styles.dropdownStyle}>
         <Picker
             selectedValue={quantityTwo}
             mode="dropdown"
             placeholder="Select quantity (other)"
             placeholderStyle={{ color: "grey" }}
             placeholderIconColor="#007aff"
             iosIcon={<Icon name="caret-down" />}
             onValueChange={(itemValue, itemIndex) => setQuantityTwo(itemValue) }         
             >
               <Picker.Item label="1" value="1" />
               <Picker.Item label="2" value="2" />
               <Picker.Item label="3" value="3" />
               <Picker.Item label="4" value="4" />
             </Picker>
             </View>
         {/** quanityt  End*/}


             <Text  onPress={()=> {
               setAddMoreStatusTwo(!addMoreStatusTwo)
               setPriceTwo(0)
               setFuelTwo('')
               setQuantityTwo('1')
             }}
              style={[styles.labelText,{color:'red',
              textDecorationLine:'underline',alignSelf:'center'}]}>Cancel</Text>

            </View>

        )
    };

    const AddMoreThree = () => {
      return(
          <View>
               {/** fuel */}
               <View  style={styles.dropdownStyle}>
           <Picker
           selectedValue={fuelThree}
           mode="dropdown"
           placeholder="LPG Gas (other)"
           placeholderStyle={{ color: "grey" }}
           placeholderIconColor="#007aff"
           iosIcon={<Icon name="caret-down" />}
           onValueChange={(itemValue, itemIndex) => {
            let obj = gases.find(o => o.id == itemValue);
            setPriceThree(obj.price)
            setFuelThree(itemValue)
           }
             }         
         >
              {
              gases.map(e =>{
               return <Picker.Item label={e.name} value={e.id} key={e.price}/>
              })
            }
           </Picker>
        </View>
       {/** fuel  End*/}



 {/** quanityt */}
 <View  style={styles.dropdownStyle}>
       <Picker
           selectedValue={quantityThree}
           mode="dropdown"
           placeholder="Select quantity (other)"
           placeholderStyle={{ color: "grey" }}
           placeholderIconColor="#007aff"
           iosIcon={<Icon name="caret-down" />}
           onValueChange={(itemValue, itemIndex) => setQuantityThree(itemValue) }         
           >
             <Picker.Item label="1" value="1" />
             <Picker.Item label="2" value="2" />
             <Picker.Item label="3" value="3" />
             <Picker.Item label="4" value="4" />
           </Picker>
           </View>
       {/** quanityt  End*/}


           <Text  onPress={()=> {
             setAddMoreStatusThree(!addMoreStatusThree)
             setPriceThree(0)
             setFuelThree('')
             setQuantityThree('1')
           }}
            style={[styles.labelText,{color:'red',
            textDecorationLine:'underline',alignSelf:'center'}]}>Cancel</Text>

          </View>

      )
    };

    const AddMoreFour = () => {
      return(
          <View>
               {/** fuel */}
               <View  style={styles.dropdownStyle}>
           <Picker
           selectedValue={fuelFour}
           mode="dropdown"
           placeholder="LPG Gas (other)"
           placeholderStyle={{ color: "grey" }}
           placeholderIconColor="#007aff"
           iosIcon={<Icon name="caret-down" />}
           onValueChange={(itemValue, itemIndex) => {
            let obj = gases.find(o => o.id == itemValue);
            setPriceFour(obj.price)
            setFuelFour(itemValue)
           }
             }         
         >
              {
              gases.map(e =>{
               return <Picker.Item label={e.name} value={e.id} key={e.price}/>
              })
            }
           </Picker>
        </View>
       {/** fuel  End*/}



 {/** quanityt */}
 <View  style={styles.dropdownStyle}>
       <Picker
           selectedValue={quantityFour}
           mode="dropdown"
           placeholder="Select quantity (other)"
           placeholderStyle={{ color: "grey" }}
           placeholderIconColor="#007aff"
           iosIcon={<Icon name="caret-down" />}
           onValueChange={(itemValue, itemIndex) => setQuantityFour(itemValue) }         
           >
             <Picker.Item label="1" value="1" />
             <Picker.Item label="2" value="2" />
             <Picker.Item label="3" value="3" />
             <Picker.Item label="4" value="4" />
           </Picker>
           </View>
       {/** quanityt  End*/}


           <Text  onPress={()=> {
             setAddMoreStatusFour(!addMoreStatusFour)
             setPriceFour(0)
             setFuelFour('')
             setQuantityFour('1')
           }}
            style={[styles.labelText,{color:'red',
            textDecorationLine:'underline',alignSelf:'center'}]}>Cancel</Text>

          </View>

      )
    };

    const AddMoreFive = () => {
      return(
          <View>
               {/** fuel */}
               <View  style={styles.dropdownStyle}>
           <Picker
           selectedValue={fuelFive}
           mode="dropdown"
           placeholder="LPG Gas (other)"
           placeholderStyle={{ color: "grey" }}
           placeholderIconColor="#007aff"
           iosIcon={<Icon name="caret-down" />}
           onValueChange={(itemValue, itemIndex) => {
            let obj = gases.find(o => o.id == itemValue);
            setPriceFive(obj.price)
            setFuelFive(itemValue)
           }
             }         
         >
              {
              gases.map(e =>{
               return <Picker.Item label={e.name} value={e.id} key={e.price}/>
              })
            }
           </Picker>
        </View>
       {/** fuel  End*/}



 {/** quanityt */}
 <View  style={styles.dropdownStyle}>
       <Picker
           selectedValue={quantityFive}
           mode="dropdown"
           placeholder="Select quantity (other)"
           placeholderStyle={{ color: "grey" }}
           placeholderIconColor="#007aff"
           iosIcon={<Icon name="caret-down" />}
           onValueChange={(itemValue, itemIndex) => setQuantityFive(itemValue) }         
           >
             <Picker.Item label="1" value="1" />
             <Picker.Item label="2" value="2" />
             <Picker.Item label="3" value="3" />
             <Picker.Item label="4" value="4" />
           </Picker>
           </View>
       {/** quanityt  End*/}


           <Text  onPress={()=> {
             setAddMoreStatusFive(!addMoreStatusFive)
             setPriceFive(0)
             setFuelFive('')
             setQuantityFive('1')
           }}
            style={[styles.labelText,{color:'red',
            textDecorationLine:'underline',alignSelf:'center'}]}>Cancel</Text>

          </View>

      )
    };

  
    function isValid(){
     var result = false
  
    if(fuel.length == 0) showMessage("Choose gas type")
    else if(quantity.length == 0) showMessage("Enter quantity")
    else if(bookDate.length == 0) showMessage("Enter date")
    else if(addMoreStatusTwo && fuelTwo.length == 0) showMessage("Choose gas type two")
    else if(addMoreStatusTwo && quantityTwo.length == 0) showMessage("Enter quantity two")
    else if(addMoreStatusThree && fuelThree.length == 0) showMessage("Choose gas type three")
    else if(addMoreStatusThree && quantityThree.length == 0) showMessage("Enter quantity three")
    else if(addMoreStatusFour && fuelFour.length == 0) showMessage("Choose gas type four")
    else if(addMoreStatusFour && quantityFour.length == 0) showMessage("Enter quantity four")
    else if(addMoreStatusFive && fuelFive.length == 0) showMessage("Choose gas type five")
    else if(addMoreStatusFive && quantityFive.length == 0) showMessage("Enter quantity five")
    
    else result = true
  
    return result
    }

    function getGas(){
      setLoading(true)
      fetch(urls.BASE_URL + 'auth/get_gases',
           {method: 'GET' }
      )
      .then((response) => response.json())
      .then(async (responseJson) => {
        setLoading(false)
          console.log("hi1",responseJson);             
          if(responseJson.status){
              setGases(responseJson.result)
          }
          else{
            showMessage(responseJson.message)
          }
         
      })
      .catch((error) => {
          setLoading(false)
          showMessage(error.message)
      });
    }


    useEffect(() =>{
        getGas()
    },[])


    function _onBook(){
   
    //  console.log(isValid())
      if(isValid()){
        let total_price = parseInt(price * quantity) + parseInt(priceTwo* quantityTwo)
        + parseInt(priceThree* quantityThree)+ parseInt(priceFour* quantityFour)+ parseInt(priceFive* quantityFive)

        let final_fuel = fuel + 
            (addMoreStatusTwo ? `,${fuelTwo}` : '') + 
            (addMoreStatusThree ? `,${fuelThree}` : '')+ 
            (addMoreStatusFour ? `,${fuelFour}` : '')+ 
            (addMoreStatusFive ? `,${fuelFive}` : '')

       let final_qty = quantity + 
            (addMoreStatusTwo ? `,${quantityTwo}` : '' ) + 
            (addMoreStatusThree ? `,${quantityThree}` : '') + 
            (addMoreStatusFour ? `,${quantityFour}` : '') + 
            (addMoreStatusFive ? `,${quantityFive}` : '')

        var formdata = new FormData()
        formdata.append('gase_id',final_fuel )
        formdata.append('qty',final_qty)
        formdata.append('user_id',userReducer.user.id)
        formdata.append('price',total_price)
        formdata.append('date',bookDate)
        formdata.append('time',time)
        formdata.append('address',props.route.params.location)
    
        console.log(JSON.stringify(formdata))
        setLoading(true)
        fetch(urls.BASE_URL + 'auth/booking',
             {method: 'POST', 
              body: formdata}
        )
        .then((response) => response.json())
        .then( (responseJson) => {
          setLoading(false)
            console.log("response--------",responseJson);             
            if(responseJson.status){
              showMessage(responseJson.message,false)
              props.navigation.navigate('home')
            }
            else{
              showMessage(responseJson.message)
            }
        })
        .catch((error) => {
            setLoading(false)
            console.log(error)
            showMessage(error.message)
        });
      }
    }
    



  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>


            <Text style={{color:'green',fontSize:20,marginVertical:10,
            alignSelf:'flex-start',marginHorizontal:10,fontWeight:'500'}}>Book Your Order</Text>


            
         {/** fuel */}
              <View  style={styles.dropdownStyle} >
             <Picker
             selectedValue={fuel}
             mode="dropdown"
             placeholder="LPG Gas"
             placeholderStyle={{ color: "grey" }}
             placeholderIconColor="#007aff"
             iosIcon={<Icon name="caret-down" />}
             onValueChange={(itemValue) => {
              setFuel(itemValue) 
              let obj = gases.find(o => o.id == itemValue);
              setPrice(obj.price)
             }}   
              >
              {
                gases.map(e =>{
                 return <Picker.Item label={e.name} value={e.id} key={e.price}/>
                })
              }
             </Picker>
             </View>
         {/** fuel  End*/}


   {/** quanityt */}
   <View  style={styles.dropdownStyle}>
         <Picker
             selectedValue={quantity}
             mode="dropdown"
             placeholder="Select quantity"
             placeholderStyle={{ color: "grey" }}
             placeholderIconColor="#007aff"
             iosIcon={<Icon name="caret-down" />}
             onValueChange={(itemValue, itemIndex) => setQuantity(itemValue) }         
            >
               <Picker.Item label="1" value="1" />
               <Picker.Item label="2" value="2" />
               <Picker.Item label="3" value="3" />
               <Picker.Item label="4" value="4" />
             </Picker>
     </View>
         {/** quanityt  End*/}



          {/** date */}
          {/* <TouchableOpacity onPress={()=> setDatePickerVisibility(!isDatePickerVisible)}
          style={styles.dateContainer}>
              {
                  bookDate 
                  ?
                     <Text style={{color:'black'}}>{bookDate}</Text>
                  :  <Text style={{color:'grey'}}>Date</Text>

              }
             
              <FastImage 
                source={require('../assets/date.png')}
                style={{height:30,width:30}} 
                resizeMode={FastImage.resizeMode.contain}/>

          </TouchableOpacity> */}

            <View style={styles.dateContainer}>
               <DatePicker
                      customStyles={{
                        dateInput: {borderWidth: 0,alignItems: "flex-start"},
                        // dateText:  {width:200},
                        // dateIcon: styles.dateIcon,
                        dateplaceholder: {
                          //alignContent: 'flex-start',
                        },
                        placeholderText: {
                          color: 'black',
                          marginLeft: '5%',
                          fontSize:17,
                          alignSelf:'flex-start'
                        },
                      }}
                      readOnly={true}
                     // style={[styles.dateContainer]}
                      date={bookDate}
                      style={{width:'100%'}}
                      mode="date"
                      placeholder={'Select date'}
                     // format="DD / MMM / YYYY"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      iconSource={require('../assets/date.png')}
                      onDateChange={date => {
                        console.log(date)
                        setBookDate(date)
                      }}
                    />
                    </View>

              {/** date end*/}


            {/** time */}
            <View  style={styles.timeContainer}>
            <RadioButton.Group onValueChange={value => setTime(value)} value={time}>
                <View style={styles.rowContainer}>
                    <Text style={styles.labelText}>Morning</Text>
                    <RadioButton.Android color={colors.COLOR_PRIMARY} value="morning" />
                </View>
                <View style={styles.rowContainer}>
                    <Text style={styles.labelText}>Afternoon</Text>
                    <RadioButton.Android color={colors.COLOR_PRIMARY} value="afternoon" />
                </View>
            </RadioButton.Group>
           </View>


              {/** time end*/}



                {/** add more two*/}
                {
                    addMoreStatusTwo
                    ?
                    <AddMoreTwo/>
                    :
                    <TouchableOpacity  onPress={()=> setAddMoreStatusTwo(!addMoreStatusTwo)}
                style={{flexDirection:'row',alignItems:'center',marginVertical:15,}}>
                        <Text style={[styles.labelText,{color:'black'}]}>Add More Products  </Text>
                        <FastImage 
                        source={require('../assets/plus.png')}
                        style={{height:20,width:20}} 
                        resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>

                }
                

               {/** add more two end*/}


                {/** add more three*/}
                {
                    addMoreStatusThree
                    ?
                    <AddMoreThree/>
                    :
                    <TouchableOpacity  onPress={()=> setAddMoreStatusThree(!addMoreStatusThree)}
                style={{flexDirection:'row',alignItems:'center',marginVertical:15,}}>
                        <Text style={[styles.labelText,{color:'black'}]}>Add More Products  </Text>
                        <FastImage 
                        source={require('../assets/plus.png')}
                        style={{height:20,width:20}} 
                        resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>

                }

               {/** add more three end*/}


                {/** add more four*/}
                {
                    addMoreStatusFour
                    ?
                    <AddMoreFour/>
                    :
                    <TouchableOpacity  onPress={()=> setAddMoreStatusFour(!addMoreStatusFour)}
                style={{flexDirection:'row',alignItems:'center',marginVertical:15,}}>
                        <Text style={[styles.labelText,{color:'black'}]}>Add More Products  </Text>
                        <FastImage 
                        source={require('../assets/plus.png')}
                        style={{height:20,width:20}} 
                        resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>

                }
               {/** add more three end*/}


               {/** add more five*/}
               {
                    addMoreStatusFive
                    ?
                    <AddMoreFive/>
                    :
                    <TouchableOpacity  onPress={()=> setAddMoreStatusFive(!addMoreStatusFive)}
                style={{flexDirection:'row',alignItems:'center',marginVertical:15,}}>
                        <Text style={[styles.labelText,{color:'black'}]}>Add More Products  </Text>
                        <FastImage 
                        source={require('../assets/plus.png')}
                        style={{height:20,width:20}} 
                        resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>

                }
               {/** add more three end*/}





                     {/** final price */}
                        <Text style={{
                            color:'black',
                            fontWeight:'900',
                            fontSize:19,
                            marginVertical:10
                        }}>Estimated Price  R {parseInt(price * quantity) + parseInt(priceTwo* quantityTwo)
                          + parseInt(priceThree* quantityThree)+ parseInt(priceFour* quantityFour)+ parseInt(priceFive* quantityFive)}</Text>

                    {/** final price end*/}







        <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
        handler={_onBook}
        label ={'Book'}/>

      

      </KeyboardAwareScrollView>

      {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flexGrow:1
      },
    input:{
        height:45,
        width:'95%',
        alignSelf:'center',
        borderRadius:10,
        borderWidth:0.8,
        borderColor:'black',
        padding:8,
        marginVertical:15,
        marginHorizontal:4,
        backgroundColor:'white',
        
    },
    dropdownStyle:{ height:50,width:dimensions.SCREEN_WIDTH * 0.9,borderRadius:10,
        borderWidth:1,borderColor:'grey',
    marginVertical:10,backgroundColor:'white'},

    dateContainer:{
        height:50,width:dimensions.SCREEN_WIDTH * 0.9,borderRadius:10,
        borderWidth:1,borderColor:'grey',
    marginVertical:10,flexDirection:'row',justifyContent:'space-between',alignItems:'center',
    paddingHorizontal:10,backgroundColor:'white'
    },

    timeContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9,borderRadius:10,
        borderWidth:1,borderColor:'grey',
    marginVertical:10,
    padding:10,backgroundColor:'white'
    },
    rowContainer:{
        flexDirection:'row',justifyContent:'space-between',alignItems:'center',
        marginVertical:5
    },
    labelText:{
        color:'grey',
        fontSize:16,
        fontWeight:'400'
    }
   

})

export default Booking;



