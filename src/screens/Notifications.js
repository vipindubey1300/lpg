import React, { useState ,useEffect, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity,FlatList
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import ProgressBar from '../components/ProgressBar';

import {colors,urls,dimensions} from '../utils/constants';

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'
import { TextInput } from 'react-native-paper';
import CustomTextInput from '../components/CustomTextInput';
import { showMessage } from '../utils/snackmsg';




const Notifications: () => React$Node = (props) => {

    const [list,setLists] = useState([]);

    const [loading, setLoading] = useState(false);
    const userReducer = useSelector(state => state.userReducer)
     const dispatch = useDispatch()


     function getNotifications(){
      var formdata = new FormData()
     
      formdata.append('user_id',userReducer.user.id)
      setLoading(true)
      fetch(urls.BASE_URL + 'auth/notifications',
           {method: 'POST',body:formdata }
      )
      .then((response) => response.json())
      .then(async (responseJson) => {
        setLoading(false)
          console.log("hi1",responseJson);             
          if(responseJson.status){
              setLists(responseJson.result)
              if(responseJson.result.length == 0) showMessage("No Notifications Found")
          }
          else{
            showMessage(responseJson.message)
          }
         
      })
      .catch((error) => {
          setLoading(false)
          showMessage(error.message)
      });
    }


    useEffect(() =>{
      getNotifications()
    },[])

 function renderItem (item)  {

    return(
        <TouchableOpacity style={styles.notificationContainer}>

            <View style={styles.notiLeft}>
            <FastImage 
            source={require('../assets/notification-black.png')}
            style={styles.icon} 
            resizeMode={FastImage.resizeMode.contain}/>
            </View>

            <View style={styles.notiRight}>
                <Text style={styles.notiText}>{item.message} </Text>
            </View>
       
        </TouchableOpacity>
    )
  }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <View
        style={styles.scrollContainer}>
                <FlatList 
                style={{marginTop:10,width:'100%',height:'100%'}}
                contentContainerStyle={{alignItems:'center'}}
               
                showsVerticalScrollIndicator={false}
                data={list}
                //renderItem={({ item }) => renderItem(item)}
               // renderItem={object =>  renderItem(object) }
                renderItem={({ item }) => renderItem(item)}
                keyExtractor={(item) => item.id } />         


      

      </View>
      </SafeAreaView>
      {loading && <ProgressBar/>}
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flexGrow:1
      },
    notificationContainer:{
        width:'95%',
        alignSelf:'center',
        backgroundColor:'white',
        borderRadius:10,
        borderColor:'grey',
        borderWidth:1,
        paddingHorizontal:10,
        paddingVertical:13,
        marginHorizontal:8,
        marginVertical:7,
        flexDirection:'row',
        alignItems:'center'
    },
    notiLeft:{
        flex:2,justifyContent:'center',alignItems:'center'
    },
    icon:{
        height:30,width:30,
    },
    notiRight:{
        flex:8
    },
    notiText:{
        color:'black'
    }
   

})

export default Notifications;
