import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity,TextInput
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import ProgressBar from '../components/ProgressBar';

import {colors,urls,dimensions} from '../utils/constants';

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'

import CustomTextInput from '../components/CustomTextInput';
import { showMessage } from '../utils/snackmsg';




const ContactAdmin: () => React$Node = (props) => {

    const [subject, setSubject ] = useState('');
    const [message, setMessage] = useState('');
    const [loading, setLoading] = useState(false);
    const userReducer = useSelector(state => state.userReducer)
     const dispatch = useDispatch()

  function isValid(){
    var result = false
   
    if(subject.length == 0) showMessage("Enter subject")
    else if(message.length == 0) showMessage("Enter message")
    else result = true
  
    return result
  }
  
  async function _onContact()  {
  
      if(isValid()){
        var formdata = new FormData()
      
        formdata.append('subject',subject)
        formdata.append('msg',message)
        formdata.append('user_id',userReducer.user.id)
        
        setLoading(true)
        fetch(urls.BASE_URL + 'auth/contact',
             {method: 'POST', 
              body: formdata}
        )
        .then((response) => response.json())
        .then(async (responseJson) => {
          setLoading(false)
            console.log("hi1",responseJson);             
            if(responseJson.status){
              showMessage(responseJson.message,false)
              props.navigation.navigate('home')
            }
            else{
              showMessage(responseJson.message)
            }
           
        })
        .catch((error) => {
            setLoading(false)
            showMessage(error.message)
        });
      }
        
       
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>


            <Text style={{color:'green',fontSize:20,marginVertical:10,alignSelf:'flex-start',marginHorizontal:10}}>Send message to admin</Text>


            <TextInput
            style={styles.input}
             value = { subject }
             placeholder={'Subject'}
             placeholderTextColor={'grey'}
             onChangeText={(value) => {
                 setSubject(value)
              }}/>
            
            <TextInput
            style={[styles.input,{height:140}]}
             value = { message }
             multiline={true}
             placeholder={'Message'}
             placeholderTextColor={'grey'}
             onChangeText={(value) => {
                setMessage(value)
              }}/>
            
   


    

        <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
        handler={_onContact}
        label ={'Contact'}/>
      

      </KeyboardAwareScrollView>
              {loading && <ProgressBar/>}
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flexGrow:1
      },
    input:{
        height:45,
        width:'95%',
        alignSelf:'center',
        borderRadius:10,
        borderWidth:0.8,
        borderColor:'black',
        padding:8,
        marginVertical:15,
        marginHorizontal:4,
        backgroundColor:'white',
        
    }
   

})

export default ContactAdmin;
