import React, { useState ,useEffect, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity,FlatList
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import ProgressBar from '../components/ProgressBar';

import {colors,urls,dimensions} from '../utils/constants';

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'
import { TextInput } from 'react-native-paper';
import CustomTextInput from '../components/CustomTextInput';
import { showMessage } from '../utils/snackmsg';




const Orders: () => React$Node = (props) => {

    const [list,setLists] = useState([]);

    const [loading, setLoading] = useState(false);
    const userReducer = useSelector(state => state.userReducer)
     const dispatch = useDispatch()


     function getOrders(){
      var formdata = new FormData()
     
      formdata.append('user_id',userReducer.user.id)
      setLoading(true)
      fetch(urls.BASE_URL + 'auth/order_history',
           {method: 'POST',body:formdata }
      )
      .then((response) => response.json())
      .then(async (responseJson) => {
        setLoading(false)
          console.log("hi1",JSON.stringify(responseJson));             
          if(responseJson.status){
             setLists(responseJson.result)
             if(responseJson.result.length == 0) showMessage("No Orders Found")

          }
          else{
            showMessage(responseJson.message)
          }
         
      })
      .catch((error) => {
          
          setLoading(false)
          showMessage(error.message)
      });
    }


    useEffect(() =>{
      getOrders()
    },[])


 function renderItem (item)  {

    return(
        <TouchableOpacity onPress={()=>props.navigation.navigate('track_order',{'item':item})}
        style={styles.orderContainer}>
            <View style={[styles.orderMain,{backgroundColor:colors.COLOR_PRIMARY}]}>
             <View style={styles.orderLeft}>
                 <Text style={styles.whiteText}>Date</Text>
                 <Text style={styles.whiteText}>:</Text>
             </View>
             <View style={styles.orderRight}>
    <Text style={styles.whiteText}>{item.date}</Text>
             </View>
            </View>


            <View style={[styles.orderMain]}>
             <View style={styles.orderLeft}>
                 <Text style={styles.blackText}>Price</Text>
                 <Text style={styles.blackText}>:</Text>
             </View>
             <View style={styles.orderRight}>
          <Text style={styles.blackText}>R {item.price}</Text>
             </View>
            </View>

            <View style={[styles.orderMain]}>
             <View style={styles.orderLeft}>
                 <Text style={styles.blackText}>Quantity</Text>
                 <Text style={styles.blackText}>:</Text>
             </View>
             <View style={styles.orderRight}>
           <Text style={styles.blackText}>{item.qty}</Text>
             </View>
            </View>
       
        </TouchableOpacity>
    )
  }
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <View style={styles.scrollContainer}>
                <FlatList 
                style={{marginTop:10,width:'100%',height:'100%'}}
                contentContainerStyle={{alignItems:'center'}}
               
                showsVerticalScrollIndicator={false}
                data={list}
                //renderItem={({ item }) => renderItem(item)}
                renderItem={({ item }) => renderItem(item)}
                keyExtractor={(item) => item.id } />         


      

      </View>
      </SafeAreaView>
      {loading && <ProgressBar/>}
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flex:1
      },
    orderContainer:{
        width:dimensions.SCREEN_WIDTH * 0.9,
        alignSelf:'center',
        backgroundColor:'white',
        borderRadius:8,
        borderColor:'grey',
        borderWidth:1,
        marginHorizontal:8,
        marginVertical:7,
        alignItems:'center',
        overflow:'hidden'
    },
    orderLeft:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',

    },
    orderRight:{
        flex:1,flexDirection:'row',marginHorizontal:10
    },
    orderMain:{
        width:'100%',padding:5,flexDirection:'row',
    },
    whiteText:{
        color:'white',
        fontSize:16
    },
    blackText:{
        color:'black',
        fontSize:16
    }
   
   

})

export default Orders;
