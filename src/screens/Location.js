import React, { useState ,forwardRef, useRef, useImperativeHandle, useEffect } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Alert,Image,TouchableOpacity,TextInput, Platform, PermissionsAndroid
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
import MapView, { Marker } from 'react-native-maps';

import {colors,urls,dimensions} from '../utils/constants';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoder';
import RBSheet from "react-native-raw-bottom-sheet";

import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'

import CustomTextInput from '../components/CustomTextInput';
import { showMessage } from '../utils/snackmsg';
import ImageInput from '../components/ImageInput';
navigator.geolocation = require('@react-native-community/geolocation');




const Location: () => React$Node = (props) => {


  const streetInput = useRef();
  const streetRef = useRef();
  const localityInput = useRef();
  const localityRef = useRef();
  const cityInput = useRef();
  const cityRef = useRef();
  const countryInput = useRef();
  const countryRef = useRef();
  const _map = useRef(null);
  const refRBSheet = useRef();

    const dispatch = useDispatch()

    const [region, setRegion] = useState({
      latitude: 30.5595,
      longitude: 22.9375,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01
    });

    const [address, setAddress] = useState('');
    const [street, setStreet] = useState('');
    const [locality, setLocality] = useState('');
    const [city, setCity] = useState('');
    const [country, setCountry] = useState('');
    const [modalVisibile, setModalVisibile] = useState(false);
    
    
   

    function _onNext(){
     
      var fulladdres = street + ',' + locality + ',' +city + ',' + country
      var fulladdresTemp = street  + locality + city + country
      if(fulladdresTemp.length == 0) showMessage("Enter location first !")
      else props.navigation.navigate('booking',{'location':fulladdres})
    }

    function _onDone(){
    
      if(street.length == 0) showMessage("Enter Street")
      else if(locality.length == 0) showMessage("Enter locality")
      else if(city.length == 0) showMessage("Enter city")
      else if(country.length == 0) showMessage("Enter country")
      else{
        var fulladdres = street + ',' + locality + ',' +city + ',' + country
        setModalVisibile(!modalVisibile)
        setAddress(fulladdres)
        setStreet(street)
        setLocality(locality)
        setCity(city)
        setCountry(country)
        //refRBSheet.current.close()
      }


     
    }


    function  getaddress(lat,long){
      console.log('getaddress------')
            var NY = { lat:lat, lng: long };
            Geocoder.geocodePosition(NY).then(res => {
              console.log(res)
              var addr  = res[0].formattedAddress
              var streetName = res[0].feature ? res[0].feature : ''
              var locality = res[0].subLocality ? res[0].subLocality : ''
              var city = res[0].locality ? res[0].locality : ''
              var country = res[0].country ? res[0].country : ''

              var fulladdres = streetName + ',' + locality + ',' +city + ',' + country
              setAddress(fulladdres)
              setStreet(streetName)
              setLocality(locality)
              setCity(city)
              setCountry(country)

            })
            .catch(err =>{
             // ToastAndroid.show("Cannot get this location !",ToastAndroid.SHORT);
              Platform.OS === 'android' 
              ? ToastAndroid.show("Cannot get this location !", ToastAndroid.SHORT)
              : Alert.alert(err.message)
            })
}



   function getCurrentLocation (){

    Geolocation.getCurrentPosition((address) => {
      console.log('--------->>>>',address)
      var lat = address.coords.latitude
      var lng = address.coords.longitude
      getaddress(lat,lng)
      setRegion({
        latitude: lat,
        longitude:lng,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01
      })


      if(_map.current) {
        _map.current.animateCamera(
          {
            center: {
              latitude:lat,
              longitude:lng
            },
            zoom: 15
          },
          3000
        );
      }
    });
   }

   async function requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          'title': 'Location Permission',
          'message': 'This App needs access to your location ' +
                     'so we can know where you are.'
        }
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use locations ")
        getCurrentLocation()
      } else {
        console.log("Location permission denied")
      }
    } catch (err) {
      console.warn(err)
    }
  }
  
 async function getLoc(){
    navigator.geolocation.getCurrentPosition(
      (address) => {
        console.log('--------->>>>',address)
        var lat = address.coords.latitude
        var lng = address.coords.longitude
        getaddress(lat,lng)
        setRegion({
          latitude: lat,
          longitude:lng,
          latitudeDelta: 0.01,
          longitudeDelta: 0.01
        })
  
  
        if(_map.current) {
          _map.current.animateCamera(
            {
              center: {
                latitude:lat,
                longitude:lng
              },
              zoom: 15
            },
            3000
          );
        }
   },
   (error) => {
       console.log('0000000---',error)
       showMessage(error.message)
      },
       { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
   );
  }

   useEffect ( () =>{
        //requestLocationPermission()
       //getCurrentLocation()
         getLoc()
    },[])

    

  

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
       
        <KeyboardAwareScrollView
        contentContainerStyle={styles.scrollContainer}>

            <Text style={{color:'green',fontSize:20,marginVertical:10,
            alignSelf:'flex-start',marginHorizontal:10,fontWeight:'500'}}>Please Enter location</Text>

            <TouchableOpacity  onPress={() => {setModalVisibile(!modalVisibile)}} 
            style={styles.addressContainer}>
              <Text>{address}</Text>
            </TouchableOpacity>
            <View style={{
                height:dimensions.SCREEN_HEIGHT * 0.3,
                width:dimensions.SCREEN_WIDTH * 0.9,
                borderRadius:15,
                overflow:'hidden'
            }}>

                <MapView
                    style={{position:'absolute',top:0,left:0,bottom:0,right:0}}
                    region={region}
                    ref={_map}
                    showsMyLocationButton={true}
                    showsUserLocation = {true}
                    followUserLocation = {true}
                    zoomEnabled = {true}
                    onRegionChangeComplete={region => setRegion(region)} >
                 <Marker 
                    coordinate={{ latitude: region.latitude, longitude:region.longitude }}
                       draggable
                       title={"Move it"}
                      onDragEnd={(e) => {
                        setRegion( {
                            latitude: e.nativeEvent.coordinate.latitude,
                            longitude:e.nativeEvent.coordinate.longitude,
                            latitudeDelta: 0.01,
                            longitudeDelta: 0.01
                          } )
                        // this.setState({latitude:e.nativeEvent.coordinate.latitude,longitude:e.nativeEvent.coordinate.longitude} )
                        getaddress(e.nativeEvent.coordinate.latitude,e.nativeEvent.coordinate.longitude)

                      }} />
                  </MapView>
            </View>

                {
                  modalVisibile && <View style={{
                    width:'100%',alignItems:'center',backgroundColor:colors.COLOR_PRIMARY,
                    marginVertical:15,borderRadius:10,paddingTop:25}}>

                      <Text style={{color:'white',
                      marginVertical:10,fontWeight:'600'}}>Enter your address manually here !</Text>

                  <TextInput
                   placeholderTextColor={'grey'}
                   placeholder={'Street'}
                   onChangeText={ (text) => setStreet(text)}
                   value={ street}
                   style={styles.input}/>
           
                     <TextInput
                   placeholderTextColor={'grey'}
                   placeholder={'Locality'}
                   onChangeText={ (text) => setLocality(text)}
                   value={ locality}
                   style={styles.input}/>
           
                 <TextInput
                   placeholderTextColor={'grey'}
                   placeholder={'City'}
                   onChangeText={ (text) => setCity(text)}
                   value={ city}
                   style={styles.input}/>
           
                   <TextInput
                   placeholderTextColor={'grey'}
                   placeholder={'Country'}
                   onChangeText={ (text) => setCountry(text)}
                   value={ country}
                   style={styles.input}/>  
              
           
                         <ButtonComponent 
                         style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19,backgroundColor:'red'}}
                         handler={_onDone}
                         label ={'Done'}/>
                    </View>
                }
           

    

        <ButtonComponent 
        style={{width :dimensions.SCREEN_WIDTH * 0.8,marginTop:35,marginBottom:19}}
        handler={_onNext}
        label ={'Next'}/>

      

      </KeyboardAwareScrollView>
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
    },
    scrollContainer:{
        padding:10,
        alignItems:'center',
        backgroundColor:colors.PAGE_BACKGROUND,
        flexGrow:1
      },
      addressContainer:{
        width:dimensions.SCREEN_WIDTH * 0.88,
        borderWidth:1,
        borderRadius:10,
        borderColor:'black',
        padding:10,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        margin:15,
        backgroundColor:'white',
        height:45
      },
      input:{
        width:'90%',
        height:45,
        borderRadius:10,
        borderColor:'white',
        borderWidth:0.7,
        padding:4,
        alignSelf:'center',
        margin:10,
        color:'black',
        backgroundColor:'white'
      }
   

})

export default Location;






