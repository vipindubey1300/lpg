import React, { useState ,forwardRef, useRef, useImperativeHandle } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,Image,TouchableOpacity,Alert
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

import {colors,urls,dimensions} from '../utils/constants';


import ButtonComponent from '../components/ButtonComponent';


import { addUser ,apiRequest,apiResponse,removeUser} from '../actions/actions';
import {useSelector, useDispatch} from 'react-redux'



const CustomDrawer : () => React$Node = (props) => {

    const userReducer = useSelector(state => state.userReducer)
    const dispatch = useDispatch()


    async function remove (){
      try {
       
       // this.props.remove({});
    
        dispatch(removeUser({}))
    
       // await AsyncStorage.removeItem("id");
        await AsyncStorage.removeItem("name");
        // await AsyncStorage.removeItem("image");
        // await AsyncStorage.removeItem("email");
        // await AsyncStorage.removeItem("phone");
        //await AsyncStorage.removeItem("lang");
        
      // props.navigation.navigate("login")
      // showMessage("Logged Out Successfully.",false)
        return true;
      }
      catch(exception) {
        
        return false;
      }
    }

    async function logout (){

      Alert.alert(
        'Logout',
        'Are you sure to logout ?',
        [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        },
        {
          text: 'OK',
          onPress: () => remove()
        }
        ],
        {
        cancelable: false
        }
      );



    }



  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}
      style={{flex:1,backgroundColor:colors.COLOR_PRIMARY}}>

      <View style={styles.headingView}>
      <FastImage 
            source={require('../assets/logo.png')}
            style={{height:'80%',width:'60%'}} 
            resizeMode={FastImage.resizeMode.contain}/>
      </View>

      


      <TouchableOpacity style={styles.labelContainer}
      onPress={()=> {
      props.navigation.navigate('home');
    }}>
        
       <Text style={styles.labelText}>Home</Text>
       <FastImage 
            source={require('../assets/arrow.png')}
            style={styles.labelImage} 
            resizeMode={FastImage.resizeMode.contain}/>
   </TouchableOpacity>



   <TouchableOpacity onPress={()=> props.navigation.navigate("orders")}
   style={styles.labelContainer}>
    <Text style={styles.labelText}>My Orders</Text>
    <FastImage 
            source={require('../assets/arrow.png')}
            style={styles.labelImage} 
            resizeMode={FastImage.resizeMode.contain}/>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=> props.navigation.navigate("notifications")}
  style={styles.labelContainer}>
    <Text style={styles.labelText}>Notifications</Text>
    <FastImage 
            source={require('../assets/arrow.png')}
            style={styles.labelImage} 
            resizeMode={FastImage.resizeMode.contain}/>
  </TouchableOpacity>




  <TouchableOpacity  onPress={()=> props.navigation.navigate("account")}
  style={styles.labelContainer}>
    <Text style={styles.labelText}>My Profile</Text>
    <FastImage 
            source={require('../assets/arrow.png')}
            style={styles.labelImage} 
            resizeMode={FastImage.resizeMode.contain}/>
  </TouchableOpacity>


  <TouchableOpacity onPress={()=> props.navigation.navigate("contact_admin")}
  style={styles.labelContainer}>
    <Text style={styles.labelText}>Contact Admin</Text>
    <FastImage 
            source={require('../assets/arrow.png')}
            style={styles.labelImage} 
            resizeMode={FastImage.resizeMode.contain}/>
  </TouchableOpacity>

  <TouchableOpacity onPress={()=> props.navigation.navigate("terms")}
  style={styles.labelContainer}>
    <Text style={styles.labelText}>Terms & Conditions</Text>
    <FastImage 
            source={require('../assets/arrow.png')}
            style={styles.labelImage} 
            resizeMode={FastImage.resizeMode.contain}/>
  </TouchableOpacity>


  <TouchableOpacity onPress={()=> logout()}
  style={styles.labelContainer}>
    <Text style={styles.labelText}>Logout</Text>
    <FastImage 
            source={require('../assets/arrow.png')}
            style={styles.labelImage} 
            resizeMode={FastImage.resizeMode.contain}/>
  </TouchableOpacity>


  

     </ScrollView>
      </SafeAreaView>
    </>
  );
};


let styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor:colors.COLOR_PRIMARY
    },
    headingView:{
        height:180,
        width:'100%',
        backgroundColor:colors.WHITE,
        justifyContent:'center',
        paddingLeft:20,alignItems:'center',
        borderColor:'blue',
        borderWidth:0.6
    },
    labelContainer:{
        height:65,
        width:'100%',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal:15,
        flexDirection:'row',
        borderBottomColor:colors.WHITE,
        borderBottomWidth:1,
        alignSelf:'center',
        marginVertical:0,
        backgroundColor:colors.COLOR_PRIMARY
    },
    labelText:{
        color:'white',
        fontSize:18,
        textAlignVertical:'center',
        fontWeight:'600'

    },
    labelImage:{
        height:27,
        width:27,
        marginRight:10
    }
   

})

export default CustomDrawer;
