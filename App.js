import React from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import store from './src/store/store';
import { Provider } from 'react-redux';
import AuthNavigator from './AuthNavigator';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native'

import { useScreens } from 'react-native-screens';

//useScreens();

const App: () => React$Node = () => {


  return (
    <Provider store={store}>
     <NavigationContainer>
        <AuthNavigator/>
      </NavigationContainer>
    </Provider>
  );
};


export default App;