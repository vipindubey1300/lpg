
import React, { useEffect, useState } from 'react';

import { NavigationContainer,DefaultTheme } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack"
import {Text, View, Image,Dimensions,ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView,Alert, LogBox} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';



import LoginNavigator from './src/navigators/LoginNavigator.js';
import DrawerNavigator from './src/navigators/DrawerNavigator.js';

import Splash from './src/screens/Splash.js';

import { addUser ,apiRequest,apiResponse} from './src/actions/actions';
import {useSelector, useDispatch} from 'react-redux'

const AuthNavigator =(props) => {
  
    LogBox.ignoreAllLogs()

    const [showSplash , setShowSplash ]  = useState(true);
    /**The equivalent of map dispatch to props is useDispatch */
    const dispatch =  useDispatch();

    /**The equivalent of map state to props is useSelector */
    const userReducer = useSelector(state => state.userReducer)

   // const loading =  useState(true);
   // const loading = useSelector(state => state.common.loading);


    const checkAuthorization = async() => {
        const name =  await AsyncStorage.getItem("name");
        const id =  await AsyncStorage.getItem("id");
        const email =  await AsyncStorage.getItem("email");
        const image =  await AsyncStorage.getItem("image");
        const mobile =  await AsyncStorage.getItem("mobile");

        let obj = {
           'id':id,
           'email':email,
           'name':name,
           'mobile':mobile,
           'image':image
       } 
         console.log("my user-------",name);
         if(name){
             //login
             dispatch(addUser(obj))
         }
         else{
            
         }
         setShowSplash(false);
        
        return name
    }

    //component did mount
    useEffect(()=> {
        console.log("User details--", userReducer)
        const timeout = setTimeout(async () => {
            checkAuthorization();
        },3000);

        //component will un-mount
        return () => {
            timeout;
        }
      
    },[]);



    

   

    if(showSplash){
         return <Splash />
    }
   

    return (
        
                 userReducer.user.name 
                 ? <DrawerNavigator /> 
                 : <LoginNavigator />
                    
                    
        
      
    )
}


export default AuthNavigator ;